﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media.Media3D;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace GcodeOlustur
{
    /*  public static T CreateDeepCopy<T>(T obj)
      {
          using (var ms = new MemoryStream())
          {

              IFormatter formatter = new BinaryFormatter();
              formatter.Serialize(ms, obj);
              ms.Seek(0, SeekOrigin.Begin);
              return (T)formatter.Deserialize(ms);
          }
      }*/
   
    public static class ExtensionMethods
    {

        public static T DeepCopy<T>(this T self)
        {

            var serialized = JsonConvert.SerializeObject(self);
            return JsonConvert.DeserializeObject<T>(serialized);
        }
    }

    [Serializable]
   static public  class YardimciObje
    {
        static public bool boruGoster=false;
        public struct siralamao
        {
            public int index;
            public double xboy;

        }

        public struct CizgiO
        {
            public Point3D Bnokta;
            public Point3D Snokta;
            public Color Renk;
            public string layer;
            public bool gizli;
            public double aci;
            public bool dis;
            public int BN;
            public int SN;
            public bool Kullan; 

        };

        public struct DaireO
        {
            public Point3D Mnokta;
            public double YariCap;
            public Color Renk;
        };

        public struct ArcO
        {
            public Point3D Mnokta;
            public double YariCap;
            public double Aci1;
            public double Aci2;
            public Color Renk;
        };

        public struct PlineO
        {
            public  List<Point3D> Nliste;
            public Color Renk;
        };


        public struct NoktaKulO
        { 
        
        
        }
        public struct NoktaO
        {
            public Point3D PointXYZ;
            public Color Renk;
            public bool dis;
            public bool gizli;
            public int No;
            public bool kullan;

        };

        public static List<NoktaO> Nokta = new List<NoktaO>();
        public static List<CizgiO> LineGcode = new List<CizgiO>();
        public static List<CizgiO> Line = new List<CizgiO>();
        public static List<DaireO> Daire = new List<DaireO>();
        public static List<ArcO> Arc = new List<ArcO>();
        public static List<PlineO> Pline = new List<PlineO>();
        public static List<List<int>> Kesim = new List<List<int>>();
        public static List<string> GcodeString = new List<string>();
        public static List<CizgiO> AnimateLine = new List<CizgiO>();
        public static  List <List<CizgiO>> TempLine = new List<List<CizgiO>>();
        public static List<CizgiO> Cizgi3d = new List<CizgiO>();
        public static List<CizgiO> SiralanmisCizgi = new List<CizgiO>();
        public static List<NoktaO> NoktaG = new List<NoktaO>();
        public static List<CizgiO> LineG = new List<CizgiO>();

        public static int indeks = 0;
        public static int DNo = 0;
    }


  

}
