﻿using System;
using HelixToolkit.Wpf;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using static GcodeOlustur.YardimciObje;

namespace GcodeOlustur
{
    /// <summary>
    /// Cizim3d.xaml etkileşim mantığı
    /// </summary>
    public partial class Cizim3d : UserControl
    {

        PointsVisual3D isaret = new PointsVisual3D();
        int secilieleman = 0;
        private GeometryModel3D SelectedModel = null;
        private Material NormalMaterial, SelectedMaterial;

        public Cizim3d()
        {
            InitializeComponent();
        }

        public void Temizle()
        { 
            MainViewPort.Children.Clear();
            HelixToolkit.Wpf.DefaultLights light = new HelixToolkit.Wpf.DefaultLights();
            MainViewPort.Children.Add(light);
         
        }

        public void Cizficiz()
        {
            Temizle();

            for (int x = 0; x < YardimciObje.Line.Count; x++)
            {
                if (YardimciObje.boruGoster == false)
                { if (YardimciObje.Line[x].gizli == false) Cizim(YardimciObje.Line[x], x); }
                else
                    Cizim(YardimciObje.Line[x], x);




            }

            for (int x = 0; x < YardimciObje.Nokta.Count; x++)
            { 
                    NoktaCiz(YardimciObje.Nokta[x], x);

            }

            MainViewPort.ZoomExtents();

            MainViewPort.IsRotationEnabled = true;
    }

        public void NoktaCiz(YardimciObje.NoktaO nokta, int No)
        {
            

            Point3D P1 =  new Point3D(nokta.PointXYZ.X/10,nokta.PointXYZ.Y/10,nokta.PointXYZ.Z/10);
            HelixToolkit.Wpf.SphereVisual3D Nokta = new HelixToolkit.Wpf.SphereVisual3D();
            Nokta.Radius = 0.04;

            Nokta.Material = Materials.Red;
            Nokta.BackMaterial = Materials.Red;

            if (nokta.dis == true)
            {
                Nokta.Material = Materials.Green;
                Nokta.BackMaterial = Materials.Green;
            }
            //Nokta.Material = new DiffuseMaterial(new SolidColorBrush(Color.FromArgb((byte)(0* 255), 100, 200, 100)));
            Nokta.SetName("NID:" + nokta.No.ToString() + " " +
                "\nX:" + Math.Round(P1.X, 3) + " \nY:" + Math.Round(P1.Y, 3) + " \nZ:" + Math.Round(P1.Z, 3));
            Nokta.Center = new Point3D(P1.X, P1.Y, P1.Z);
            Nokta.PhiDiv = 6;
            MainViewPort.Children.Add(Nokta);

        }

        public void Cizim(YardimciObje.CizgiO cizgi,int No)
        {
            Point3D p1 = new Point3D();
            Point3D p2= new Point3D();
            p1.X = cizgi.Bnokta.X/10;
            p1.Y = cizgi.Bnokta.Y/10;
            p1.Z = cizgi.Bnokta.Z/10;

            p2.X = cizgi.Snokta.X/10;
            p2.Y = cizgi.Snokta.Y/10;
            p2.Z = cizgi.Snokta.Z/10;

            Point3DCollection linepoints1 = new Point3DCollection();
            linepoints1.Add(p1);
            linepoints1.Add(p2);
            LinesVisual3D CubukTel = new LinesVisual3D();
            CubukTel.Points = linepoints1;
            CubukTel.Thickness = 1;
            if (cizgi.gizli == false)
            {
                if (cizgi.dis == false)
                {
                    CubukTel.Thickness = 2;
                    CubukTel.Color = Colors.Red;
                }
                else
                {
                    CubukTel.Thickness = 2;
                    CubukTel.Color = Colors.Green;
                }
            }
            else
                CubukTel.Color = Colors.Yellow;

            CubukTel.SetName("CID:"+No.ToString() + " \n Bn:"+ cizgi.BN.ToString() + " \n Sn:" + cizgi.SN.ToString());
            MainViewPort.Children.Add(CubukTel);


        }

        private void StyleTexblock(Border txt)
        {
            txt.BorderThickness = new Thickness(3);
            txt.Padding = new Thickness(3);
            //txt.Background = Brushes.White;
            txt.BorderBrush = Brushes.Gray;

            txt.CornerRadius = new CornerRadius(10);
        }
        private void MainViewPort_MouseMove(object sender, MouseEventArgs e)
        {
          

        Point mouse_pos = e.GetPosition(MainViewPort);


            double x = mouse_pos.X;
            double y = mouse_pos.Y;


            tb.Visibility = Visibility.Hidden;
            tb.Margin = new Thickness(x + 25, y + 5, 0, 0);
            StyleTexblock(tb);

            var viewport = (HelixViewport3D)sender;
            var firstHit = viewport.Viewport.FindHits(e.GetPosition(viewport)).FirstOrDefault();
            try
            {
                // var firstHit = ;
              
                    for (int c = 0; c < viewport.Viewport.FindHits(e.GetPosition(viewport)).Count; c++)
                    {
                        if (firstHit.Visual.DependencyObjectType.Name == "SphereVisual3D") continue;
                        //if (firstHit.Visual.DependencyObjectType.Name == "PointsVisual3D") continue;
                        firstHit = viewport.Viewport.FindHits(e.GetPosition(viewport))[c];

                    }
              

                var yellowMaterial = MaterialHelper.CreateMaterial(Colors.Yellow);
                if (firstHit != null && firstHit.Visual.DependencyObjectType.Name != "PointsVisual3D")
                //  if (firstHit != null )
                {
                    double XX = firstHit.Position.X;
                    double YY = firstHit.Position.Y;
                    double ZZ = firstHit.Position.Z;

                    //         Console.WriteLine("X={0}\n Y={1}\n Z={2}\n", XX, YY, ZZ);
                    isaret.Size = 10;

                    var isim = firstHit.Visual.GetName();
                    if (isim != null)
                    {
                        // Ekranyazi.Visibility = Visibility.Visible;
                        //Ekranyazi.Content = isim;
                        tb.Visibility = Visibility.Visible;
                        tx.Content = isim;
                        tb.Width = tx.Width;
                        tb.Height = tx.Height;
                    }

                    //         
                }

 

                if (firstHit != null) //Boş değilse
                                      //Düşünülecek
                                      //if (selectedModels.Contains((Model3D)firstHit.Model) == false) 
                {

                    if (secilieleman == 0)
                    {
                        // Deselect the prevously selected model.
                        if (SelectedModel != null )
                        {
                            SelectedModel.BackMaterial = NormalMaterial;
                            SelectedModel.Material = NormalMaterial;
                            SelectedModel = null;

                        }


                        if (firstHit != null)
                        {
                            SelectedMaterial = new DiffuseMaterial(Brushes.Yellow);
                            var Center = HelixToolkit.Wpf.SphereVisual3D.CenterProperty;
                            GeometryModel3D model1 = (GeometryModel3D)firstHit.Model;
                            Point3D merkez = (Point3D)firstHit.Visual.GetValue(Center);
                            string elemanismiFull = (string)firstHit.Visual.GetName();
                            SelectedModel = model1;
                             NormalMaterial = SelectedModel.Material;
                            SelectedModel.Material = SelectedMaterial;
                            SelectedModel.BackMaterial = SelectedMaterial;  
                        }
                    }



                }


            }
            catch { }
        }
    }
}
