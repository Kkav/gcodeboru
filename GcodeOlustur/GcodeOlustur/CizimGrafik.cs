﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GcodeOlustur
{
    public partial class CizimGrafik : UserControl
    {

        Graphics grafik;

        public double carpan = 1.0;
        public double KaydirX = 0, KaydirY = 0;
        public double KayilkX = 0, KayilkY = 0;
        public double XX = 0, YY = 0, ZZ = 0;
        private Rectangle SecimRect;
        private Rectangle oldRect;
        private int dragHandle = 0;
        private Point dragPoint;
   
        public int Mod = 0;
        YardimciObje.CizgiO[] sec = new YardimciObje.CizgiO[2];



        public CizimGrafik()
        {
            InitializeComponent();
        }

        private void CizimGrafik_Load(object sender, EventArgs e)
        {
            grafik = this.CreateGraphics();
            KaydirX = 2;
            KaydirY = -500;
        }


        protected override void OnMouseDown(MouseEventArgs e)
        {
            XX = Math.Round(e.Location.X / carpan + KaydirX, 3);
            YY = Math.Round(e.Location.Y / carpan + KaydirY, 3);
            
            if (Mod == 1 || Mod==2)
            {
                KesisimNoktalari(XX, YY);
                //Secim();
            }

            if (e.Button == MouseButtons.Middle)
            {
                KayilkX = e.X;
                KayilkY = e.Y;
                Cursor.Current = Cursors.Hand;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                KaydirX = KaydirX + (KayilkX - e.X) / carpan;
                KaydirY = KaydirY + (KayilkY - e.Y) / carpan;
                KayilkX = 0;
                KayilkY = 0;

                Cursor.Current = Cursors.Default;
                this.Refresh();
            }
        }


        protected override void OnMouseWheel(MouseEventArgs e)
        {

            if (e.Delta == 120)
                carpan = carpan + 0.005;
            else
                carpan = carpan - 0.005;

            if (carpan < 0.005) carpan = 0.005;
            if (carpan > 100000) carpan = 100000;

            this.Refresh();
            base.OnMouseWheel(e);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Mod = 1;
        }
        private void Secim()
        { 
        
        }

        private void KesisimNoktalari(double Xm, double Ym)
        {
            double Xs = 0;
            double Ys = 0;
            //Cizgi Noktaları
           

            sec[0].Bnokta.X = Xm - 2 / carpan;
            sec[0].Bnokta.Y = Ym - 2 / carpan;

            sec[0].Snokta.X = Xm + 2 / carpan;
            sec[0].Snokta.Y = Ym + 2 / carpan;


            sec[1].Bnokta.X = Xm + 2 / carpan;
            sec[1].Bnokta.Y = Ym - 2 / carpan;

            sec[1].Snokta.X = Xm - 2 / carpan;
            sec[1].Snokta.Y = Ym + 2 / carpan;


            for (int x = 0; x < YardimciObje.Line.Count; x++)
            {


                for (int y = 0; y < 2; y++)
                {

                    double X1 = YardimciObje.Line[x].Bnokta.X;
                    double Y1 = YardimciObje.Line[x].Bnokta.Y;
                    double X2 = YardimciObje.Line[x].Snokta.X;
                    double Y2 = YardimciObje.Line[x].Snokta.Y;

                    double DX1 = sec[y].Bnokta.X;
                    double DY1 = sec[y].Bnokta.Y;
                    double DX2 = sec[y].Snokta.X;
                    double DY2 = sec[y].Snokta.Y;

                    double a = Y1 - Y2;
                    double b = X1 - X2;

                    double a1 = DY1 - DY2;
                    double b1 = DX1 - DX2;

                    if (b != 0 && b1 != 0)
                    {
                        Xs = (DY1 - Y1 + a / b * X1 - a1 / b1 * DX1) / (a / b - a1 / b1);
                        Ys = (Xs - X1) * a / b + Y1;
                    }
                    else if (b == 0 && b1 != 0)
                    {
                        Xs = X1;
                        Ys = (Xs - DX1) * a1 / b1 + DY1;
                    }

                    else if (b != 1 && b1 == 0)
                    {
                        Xs = DX1;
                        Ys = (Xs - X1) * a / b + Y1;
                    }

                    else if (b == 0 && b1 == 0)
                    {
                        continue;
                    }

                    //Kesişim aralığı belirleme

                    double uzunluk = Math.Sqrt(Math.Pow(X1 - X2, 2) + Math.Pow(Y1 - Y2, 2));
                    double uzunluk1 = Math.Sqrt(Math.Pow(X1 - Xs, 2) + Math.Pow(Y1 - Ys, 2));
                    double uzunluk2 = Math.Sqrt(Math.Pow(Xs - X2, 2) + Math.Pow(Ys - Y2, 2));

                    double uzunlukx = Math.Sqrt(Math.Pow(DX1 - DX2, 2) + Math.Pow(DY1 - DY2, 2));
                    double uzunlukx1 = Math.Sqrt(Math.Pow(DX1 - Xs, 2) + Math.Pow(DY1 - Ys, 2));
                    double uzunlukx2 = Math.Sqrt(Math.Pow(Xs - DX2, 2) + Math.Pow(Ys - DY2, 2));

                    if (uzunluk >= uzunluk1 && uzunluk >= uzunluk2 && uzunlukx >= uzunlukx1 && uzunlukx >= uzunlukx2)
                    {

                        List<YardimciObje.CizgiO> TLine = ExtensionMethods.DeepCopy(YardimciObje.Line);

                        YardimciObje.TempLine.Add(TLine);
                        YardimciObje.indeks = YardimciObje.TempLine.Count-1;

                        if (Mod == 1)
                        {
                            YardimciObje.CizgiO temp = new YardimciObje.CizgiO();
                            temp = YardimciObje.Line[x];
                            if (YardimciObje.Line[x].layer != "kes")
                            {

                                temp.layer = "kes";
                                YardimciObje.Line[x] = temp;
                            }
                            else
                            {

                                temp.layer = "0";
                                YardimciObje.Line[x] = temp;
                            }
                            Refresh();
                            return;

                        }
                //Silme Yeri
                    if (Mod == 2)
                    {
                        YardimciObje.Line.RemoveAt(x);
                            YardimciObje.LineGcode.Clear();
                            YardimciObje.Kesim.Clear();
                            YardimciObje.AnimateLine.Clear();
                            Refresh();
                        return;

                    }
                }
            }
            }


        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            carpan = carpan + 0.05;
            if (carpan < 0.05) carpan = 0.05;
            if (carpan > 100000) carpan = 100000;
            Refresh();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            carpan = carpan - 0.05;
            if (carpan < 0.05) carpan = 0.05;
            if (carpan > 100000) carpan = 100000;
            Refresh();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            carpan = 1;
            KaydirX = 0;
            KaydirY = 0;


            Refresh();

        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            Mod = 2;//Sil
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            YardimciObje.indeks = YardimciObje.indeks - 1;
            if (YardimciObje.indeks < -1) YardimciObje.indeks = -1;

            if (YardimciObje.TempLine.Count > 0 && YardimciObje.indeks>-1)
            {
                YardimciObje.Line = YardimciObje.TempLine[YardimciObje.indeks];
              
                Refresh();
            }
          
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            if (YardimciObje.TempLine.Count > 0 && YardimciObje.indeks > -1 && YardimciObje.indeks<=YardimciObje.TempLine.Count-1)
            {
                YardimciObje.Line = YardimciObje.TempLine[YardimciObje.indeks];
              
                Refresh();
            }
            YardimciObje.indeks = YardimciObje.indeks + 1;
            if (YardimciObje.indeks > YardimciObje.TempLine.Count - 1) YardimciObje.indeks = YardimciObje.TempLine.Count - 1;

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Refresh();
        }









        protected override void OnMouseMove(MouseEventArgs e)
        {
       //     this.Refresh();
            Point localx = new Point(e.Location.X + 5, e.Location.Y - 10);
            //Konum bilgisi
            XX = Math.Round(e.Location.X / carpan + KaydirX, 3);
            YY = Math.Round(e.Location.Y / carpan + KaydirY, 3);

            XYZLabel.Text = "X:" + XX + " Y:" + YY;

            ZZ = 0;
            if (Mod == 1 || Mod == 2)
            {
               
                sec[0].Bnokta.X = XX - 2 / carpan;
                sec[0].Bnokta.Y = YY - 2 / carpan;
                sec[0].Snokta.X = XX + 2 / carpan;
                sec[0].Snokta.Y = YY + 2 / carpan;
                sec[1].Bnokta.X = XX + 2 / carpan;
                sec[1].Bnokta.Y = YY - 2 / carpan;
                sec[1].Snokta.X = XX - 2 / carpan;
                sec[1].Snokta.Y = YY + 2 / carpan;
                Refresh();
            }


         


            if (Mod == 5)
            {
                if (dragHandle == 1)
                {
                    int diffX = dragPoint.X - e.Location.X;
                    int diffY = dragPoint.Y - e.Location.Y;
                    SecimRect = new Rectangle(oldRect.Left - diffX, oldRect.Top - diffY, oldRect.Width + diffX, oldRect.Height + diffY);
                }
                else if (dragHandle == 2)
                {
                    int diff = dragPoint.X - e.Location.X;
                    SecimRect = new Rectangle(oldRect.Left - diff, oldRect.Top, oldRect.Width + diff, oldRect.Height);
                }
                else if (dragHandle == 3)
                {
                    int diffX = dragPoint.X - e.Location.X;
                    int diffY = dragPoint.Y - e.Location.Y;
                    SecimRect = new Rectangle(oldRect.Left - diffX, oldRect.Top, oldRect.Width + diffX, oldRect.Height - diffY);
                }
                else if (dragHandle == 4)
                {
                    int diffX = dragPoint.X - e.Location.X;
                    int diffY = dragPoint.Y - e.Location.Y;
                    SecimRect = new Rectangle(oldRect.Left, oldRect.Top - diffY, oldRect.Width, oldRect.Height + diffY);
                }
                else if (dragHandle == 5)
                {
                    int diffX = dragPoint.X - e.Location.X;
                    int diffY = dragPoint.Y - e.Location.Y;
                    SecimRect = new Rectangle(oldRect.Left, oldRect.Top, oldRect.Width, oldRect.Height - diffY);
                }
                else if (dragHandle == 6)
                {
                    int diffX = dragPoint.X - e.Location.X;
                    int diffY = dragPoint.Y - e.Location.Y;
                    SecimRect = new Rectangle(oldRect.Left, oldRect.Top - diffY, oldRect.Width - diffX, oldRect.Height + diffY);
                }
                else if (dragHandle == 7)
                {
                    int diff = dragPoint.X - e.Location.X;
                    SecimRect = new Rectangle(oldRect.Left, oldRect.Top, oldRect.Width - diff, oldRect.Height);
                }
                else if (dragHandle == 8)
                {
                    int diffX = dragPoint.X - e.Location.X;
                    int diffY = dragPoint.Y - e.Location.Y;
                    SecimRect = new Rectangle(oldRect.Left, oldRect.Top, oldRect.Width - diffX, oldRect.Height - diffY);
                }

                if (dragHandle > 0)
                    this.Invalidate();


            }


            base.OnMouseMove(e);
        }

        public void GrafikCiz()
        {

            grafik.Clear(Color.Black);


            SolidBrush isaret = new SolidBrush(Color.Red);
            float X1 = new float();
            float Y1 = new float();
            float X2 = new float();
            float Y2 = new float();

            if (Mod == 1 || Mod == 2)
            {
                Pen kalem = new Pen(Color.BurlyWood);
                kalem.Width = (float)0.1;

                for (int y = 0; y < 2; y++)
                {


                    X1 = (float)((sec[y].Bnokta.X - KaydirX) * carpan);
                    Y1 = (float)((sec[y].Bnokta.Y - KaydirY) * carpan);
                    X2 = (float)((sec[y].Snokta.X - KaydirX) * carpan);
                    Y2 = (float)((sec[y].Snokta.Y - KaydirY) * carpan);
                    grafik.DrawLine(kalem, X1, Y1, X2, Y2);

                }

            }


            SolidBrush yazim = new SolidBrush(Color.White);
            for (int x = 0; x < YardimciObje.Line.Count; x++)
            {
                Pen kalem = new Pen(Color.Yellow);
                kalem.Width = (float)0.1;

                if (YardimciObje.Line[x].layer == "kes")
                {
                    kalem.Width = (float)0.2;
                    kalem.Color = Color.PowderBlue;

                }




                X1 = (float)((YardimciObje.Line[x].Bnokta.X - KaydirX) * carpan);
                Y1 = (float)((YardimciObje.Line[x].Bnokta.Y - KaydirY) * carpan);
                X2 = (float)((YardimciObje.Line[x].Snokta.X - KaydirX) * carpan);
                Y2 = (float)((YardimciObje.Line[x].Snokta.Y - KaydirY) * carpan);
                grafik.DrawLine(kalem, X1, Y1, X2, Y2);
                //GraphicsExtensionsX.FillCircle(e.Graphics, isaret, X1, Y1, 5);
                //GraphicsExtensionsX.FillCircle(e.Graphics, isaret, X2, Y2, 5);

            }



            if (Mod == 0)
            {

                for (int t = 0; t < YardimciObje.Kesim.Count; t++)
                //for (int t = 4; t < 5; t++)
                {
                    for (int m = 0; m < YardimciObje.Kesim[t].Count; m++)
                    {
                        int x = YardimciObje.Kesim[t][m];
                        //if (x == -1) continue;
                        Pen kalem = new Pen(Color.Red);//YardimciObje.Line[x].Renk);// ;

                        kalem.Width = (float)2.0;



                        X1 = (float)((YardimciObje.LineGcode[x].Bnokta.X - KaydirX) * carpan);
                        Y1 = (float)((YardimciObje.LineGcode[x].Bnokta.Y - KaydirY) * carpan);
                        X2 = (float)((YardimciObje.LineGcode[x].Snokta.X - KaydirX) * carpan);
                        Y2 = (float)((YardimciObje.LineGcode[x].Snokta.Y - KaydirY) * carpan);
                        grafik.DrawLine(kalem, X1, Y1, X2, Y2);
                        if (checkBox1.Checked == true)
                        {
                            GraphicsExtensions.FillCircle(grafik, isaret, X1, Y1, (float)5.0);
                            GraphicsExtensions.FillCircle(grafik, isaret, X2, Y2, (float)5.0);
                        }
                    }
                }


                //Animate

                for (int x = 0; x < YardimciObje.AnimateLine.Count; x++)
                {

                    //if (x == -1) continue;
                    Pen kalem = new Pen(YardimciObje.AnimateLine[x].Renk);//YardimciObje.Line[x].Renk);// ;
                    kalem.Width = (float)2.0;

                    X1 = (float)((YardimciObje.AnimateLine[x].Bnokta.X - KaydirX) * carpan);
                    Y1 = (float)((YardimciObje.AnimateLine[x].Bnokta.Y - KaydirY) * carpan);
                    X2 = (float)((YardimciObje.AnimateLine[x].Snokta.X - KaydirX) * carpan);
                    Y2 = (float)((YardimciObje.AnimateLine[x].Snokta.Y - KaydirY) * carpan);
                    grafik.DrawLine(kalem, X1, Y1, X2, Y2);
                    PointF yazixy = new PointF((X1 + X2) / 2, (Y1 + Y2) / 2);

                    if (checkBox2.Checked == true)
                    {
                        Font drawFont = new Font("Arial", 8);
                        grafik.DrawString(x.ToString(), drawFont, yazim, yazixy);
                    }

                }
            }



        }

        protected override void OnPaint(PaintEventArgs e)
        {
           GrafikCiz();
        }


        private Point GetHandlePoint(int value)
        {
            Point result = Point.Empty;

            if (value == 1)
                result = new Point(SecimRect.Left, SecimRect.Top);
            else if (value == 2)
                result = new Point(SecimRect.Left, SecimRect.Top + (SecimRect.Height / 2));
            else if (value == 3)
                result = new Point(SecimRect.Left, SecimRect.Bottom);
            else if (value == 4)
                result = new Point(SecimRect.Left + (SecimRect.Width / 2), SecimRect.Top);
            else if (value == 5)
                result = new Point(SecimRect.Left + (SecimRect.Width / 2), SecimRect.Bottom);
            else if (value == 6)
                result = new Point(SecimRect.Right, SecimRect.Top);
            else if (value == 7)
                result = new Point(SecimRect.Right, SecimRect.Top + (SecimRect.Height / 2));
            else if (value == 8)
                result = new Point(SecimRect.Right, SecimRect.Bottom);

            return result;
        }

        private Rectangle GetHandleRect(int value)
        {
            Point p = GetHandlePoint(value);
            p.Offset(-2, -2);
            return new Rectangle(p, new Size(5, 5));
        }

        public void CizgiCiz(double X1,double Y1, double X2,double Y2,Color renk)
        {
            
            ///SolidBrush isaret = new SolidBrush(renk);
            /*Pen kalem = new Pen(Color.Red);
            X1 = (float)((X1 - KaydirX) * carpan);
            Y1 = (float)((Y1 - KaydirY) * carpan);
            X2 = (float)((X2 - KaydirX) * carpan);
            Y2 = (float)((Y2 - KaydirY) * carpan);
            e.Graphics.DrawLine(kalem, X1, Y1, X2, Y2);
            */
            
        }

    }
    public static class GraphicsExtensions
    {
        public static void DrawCircle(this Graphics g, Pen pen,
                                      float centerX, float centerY, float radius)
        {
            g.DrawEllipse(pen, centerX - radius, centerY - radius,
                          radius + radius, radius + radius);
        }

        public static void FillCircle(this Graphics g, Brush brush,
                                      float centerX, float centerY, float radius)
        {
            g.FillEllipse(brush, centerX - radius, centerY - radius,
                          radius + radius, radius + radius);
        }
    }
}
