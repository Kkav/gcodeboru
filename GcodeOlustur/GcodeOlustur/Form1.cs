﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Media3D;
namespace GcodeOlustur
{
   


    public partial class AnaPencere : Form
    {
        struct PoliO
        {
            public  double x;
            public double y;
            public double z;
            public bool sonuc;
        }

        List<YardimciObje.siralamao> siralaKucuk = new List<YardimciObje.siralamao>();

        int sonbas = -1;
        double maxy = 0;
        double maxx = 0;
        double minx = 9999999999999;
        double yOrt = 0, zOrt = 0;
        double maxX = 0, minX = 0, maxY = 0, minY = 0, maxZ = 0, minZ = 0;
        double cap = 0;
        int KesimAdet = 0;
        double DisCap = 0;
        double icCap = 0;
        double kalinlik;
        double UcMesafe = 0;
        public AnaPencere()
        {
            InitializeComponent();
        }

 

        private void groupBox1_Enter(object sender, EventArgs e)
        {
  
        }

        private void AnaPencere_Load(object sender, EventArgs e)
        {
            UcMesafe = Convert.ToDouble(Uc_mesafe.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DXFAc();
        }
        private double [] MatrisCarpim(double [,]Rm ,double [] Mm )

        {
            double topla = 0;
            double[] Rg = new double[4];
            for (int i = 0; i < 4; i++)
            { topla = 0;
        for (int j = 0; j < 4; j++)
            {
                topla = topla + Rm[i, j] * Mm[j];
            }
            Rg[i] = topla;
         }

            return Rg;
}
        private void ObjeCevir()
        {

            double[,] Rz = new double[4, 4];
            double[,] Ry = new double[4, 4];
            double teta=0;

            // max uzunluğu bul
            double uzk = 0;
            int indeks = 0;
            double Xm, Ym, Zm, l; 


 for (int Pnumara = 0; Pnumara < YardimciObje.Line.Count; Pnumara++)
            {
                 Xm = YardimciObje.Line[Pnumara].Bnokta.X - YardimciObje.Line[Pnumara].Snokta.X;
                 Ym = YardimciObje.Line[Pnumara].Bnokta.Y - YardimciObje.Line[Pnumara].Snokta.Y;
                 Zm = YardimciObje.Line[Pnumara].Bnokta.Z - YardimciObje.Line[Pnumara].Snokta.Z;
                 l = Math.Sqrt(Xm * Xm + Ym * Ym + Zm * Zm);
                if (l > uzk) { uzk=l; indeks = Pnumara; }
            }

              Xm = YardimciObje.Line[indeks].Snokta.X - YardimciObje.Line[indeks].Bnokta.X;
              Ym = YardimciObje.Line[indeks].Snokta.Y - YardimciObje.Line[indeks].Bnokta.Y;
              Zm = YardimciObje.Line[indeks].Snokta.Z - YardimciObje.Line[indeks].Bnokta.Z;

            l = Math.Sqrt(Xm * Xm + Ym * Ym + Zm * Zm);
            if (Xm == 0) Xm = 0.001;
            double a1 = -Ym / Xm;
            teta = Math.Atan(a1);
            double aci1 = teta * 180 / Math.PI;


            // Z etrafinda cevir
            Rz[0, 0] = Math.Cos(teta);
            Rz[0, 1] = -Math.Sin(teta);
            Rz[0, 2] = 0;
            Rz[0, 3] = 0;

            Rz[1, 0] = Math.Sin(teta);
            Rz[1, 1] = Math.Cos(teta);
            Rz[1, 2] = 0;
            Rz[1, 3] = 0;

            Rz[2, 0] = 0;
            Rz[2, 1] = 0;
            Rz[2, 2] = 1;
            Rz[2, 3] = 0;

            Rz[3, 0] = 0;
            Rz[3, 1] = 0;
            Rz[3, 2] = 0;
            Rz[3, 3] = 1;



 for (int i = 0; i < YardimciObje.Line.Count; i++)
            {
double[] M = new double[4];
                M[0] = YardimciObje.Line[i].Bnokta.X;
                M[1] = YardimciObje.Line[i].Bnokta.Y;
                M[2] = YardimciObje.Line[i].Bnokta.Z;
                M[3] = 1;
var Rg=MatrisCarpim(Rz, M);
                M[0] = YardimciObje.Line[i].Snokta.X;
                M[1] = YardimciObje.Line[i].Snokta.Y;
                M[2] = YardimciObje.Line[i].Snokta.Z;
                M[3] = 1;
var Rg2 = MatrisCarpim(Rz, M);
                YardimciObje.CizgiO temp = YardimciObje.Line[i];
                temp.Bnokta.X = Rg[0];
                temp.Bnokta.Y = Rg[1];
                temp.Bnokta.Z = Rg[2];
                temp.Snokta.X = Rg2[0];
                temp.Snokta.Y = Rg2[1];
                temp.Snokta.Z = Rg2[2];
                YardimciObje.Line[i] = temp;
            }


            uzk = 0;
            for (int Pnumara = 0; Pnumara < YardimciObje.Line.Count; Pnumara++)
            {
                Xm = YardimciObje.Line[Pnumara].Bnokta.X - YardimciObje.Line[Pnumara].Snokta.X;
                Ym = YardimciObje.Line[Pnumara].Bnokta.Y - YardimciObje.Line[Pnumara].Snokta.Y;
                Zm = YardimciObje.Line[Pnumara].Bnokta.Z - YardimciObje.Line[Pnumara].Snokta.Z;
                l = Math.Sqrt(Xm * Xm + Ym * Ym + Zm * Zm);
                if (l > uzk) { uzk = l; indeks = Pnumara; }
            }

            Xm = YardimciObje.Line[indeks].Snokta.X - YardimciObje.Line[indeks].Bnokta.X;
            Ym = YardimciObje.Line[indeks].Snokta.Y - YardimciObje.Line[indeks].Bnokta.Y;
            Zm = YardimciObje.Line[indeks].Snokta.Z - YardimciObje.Line[indeks].Bnokta.Z;

            double a2 = Zm / Xm;
            double beta = Math.Atan(a2);
            double aci2 = beta * 180 / Math.PI;

            // Y etrafında cevir
            Ry[0, 0] = Math.Cos(beta);
            Ry[0, 1] = 0;
            Ry[0, 2] = Math.Sin(beta);
            Ry[0, 3] = 0;

            Ry[1, 0] = 0;
            Ry[1, 1] = 1;
            Ry[1, 2] = 0;
            Ry[1, 3] = 0;

            Ry[2, 0] = -Math.Sin(beta);
            Ry[2, 1] = 0;
            Ry[2, 2] = Math.Cos(beta);
            Ry[2, 3] = 0;

            Ry[3, 0] = 0;
            Ry[3, 1] = 0;
            Ry[3, 2] = 0;
            Ry[3, 3] = 1;

            for (int i = 0; i < YardimciObje.Line.Count; i++)
            {
                double[] M = new double[4];
                M[0] = YardimciObje.Line[i].Bnokta.X;
                M[1] = YardimciObje.Line[i].Bnokta.Y;
                M[2] = YardimciObje.Line[i].Bnokta.Z;
                M[3] = 1;
                var Rg = MatrisCarpim(Ry, M);

                M[0] = YardimciObje.Line[i].Snokta.X;
                M[1] = YardimciObje.Line[i].Snokta.Y;
                M[2] = YardimciObje.Line[i].Snokta.Z;
                M[3] = 1;
                var Rg2 = MatrisCarpim(Ry, M);

                YardimciObje.CizgiO temp = YardimciObje.Line[i];
                temp.Bnokta.X = Rg[0];
                temp.Bnokta.Y = Rg[1];
                temp.Bnokta.Z = Rg[2];

                temp.Snokta.X = Rg2[0];
                temp.Snokta.Y = Rg2[1];
                temp.Snokta.Z = Rg2[2];
                YardimciObje.Line[i] = temp;
            }

        }
        private void DXFAc3D()
        {
            
            try
            {
                openFileDialog1.ShowDialog();
                if (openFileDialog1.FileName != "")
                {
            //        AnaPencere.ActiveForm.Text = "Boru Gcode Oluşturma  " + openFileDialog1.FileName;
                    YardimciObje.LineGcode.Clear();
                    YardimciObje.Kesim.Clear();
                    YardimciObje.Line.Clear();
                    YardimciObje.Daire.Clear();
                    YardimciObje.Arc.Clear();
                    YardimciObje.Pline.Clear();
                    YardimciObje.AnimateLine.Clear();
                    listBox1.Items.Clear();

                    FileStream fs = new System.IO.FileStream(
                        openFileDialog1.FileName,
                        System.IO.FileMode.Open,
                        System.IO.FileAccess.Read);

                    StreamReader sr = new StreamReader(fs);

                    while (sr.Peek() != -1)
                    {
                        string buf = sr.ReadLine();
                       // Console.WriteLine(buf);


                        if (buf == "AcDbFace")
                        { 

                            {


                                double Tx = 0, Ty = 0, Tz = 0;
                                double Txx = 0, Tyy = 0, Tzz = 0;
                                PoliO[] Poligon = new PoliO[4];
                                bool[] gizle = new bool[4];
                                int gizli = 0;
                                YardimciObje.CizgiO temp = new YardimciObje.CizgiO();
                                temp.Renk = Color.LightYellow;
                                temp.layer = "0";// buf.Trim().ToLower(); //kes layeri



                                for (int i = 0; i < 4; i++)
                                {

                                    buf = sr.ReadLine();//1x
                                    buf = sr.ReadLine();
                                    Poligon[i].x = double.Parse(buf);

                                    buf = sr.ReadLine();
                                    buf = sr.ReadLine();//2x
                                    Poligon[i].y = double.Parse(buf);

                                    buf = sr.ReadLine();//3x
                                    buf = sr.ReadLine();
                                    Poligon[i].z = double.Parse(buf);
                                }
                                    buf = sr.ReadLine();
                                if (buf.Trim() == "70")
                                {
                                    buf = sr.ReadLine();
                                    gizli = int.Parse(buf);
                                }
                                else
                                gizli = 0;



                                    int Mod = gizli% 8;

                                    if (Mod < gizli) gizle[3] = true;

                                    int Mod2 = Mod % 4;

                                    if (Mod2 <Mod) gizle[2] = true;

                                    int Mod3 = Mod2 % 2;

                                    if (Mod3<Mod2) gizle[1] = true;

                                    int Mod4 = Mod3 % 1;

                                    if (Mod4 < Mod3) gizle[0] = true;


                                    if (gizle[0] == false)
                                    {
                                        temp = new YardimciObje.CizgiO();
                                        temp.Renk = Color.LightYellow;
                                        temp.layer = "0";

                                        temp.Bnokta.X = Poligon[0].x;
                                        temp.Bnokta.Y = Poligon[0].y;
                                        temp.Bnokta.Z = Poligon[0].z;

                                        temp.Snokta.X = Poligon[1].x;
                                        temp.Snokta.Y = Poligon[1].y;
                                        temp.Snokta.Z = Poligon[1].z;
                                        YardimciObje.Line.Add(temp);
                                    }

                                    if (gizle[1] == false)
                                    {
                                        temp = new YardimciObje.CizgiO();
                                        temp.Renk = Color.LightYellow;
                                        temp.layer = "0";

                                        temp.Bnokta.X = Poligon[1].x;
                                        temp.Bnokta.Y = Poligon[1].y;
                                        temp.Bnokta.Z = Poligon[1].z;

                                        temp.Snokta.X = Poligon[2].x;
                                        temp.Snokta.Y = Poligon[2].y;
                                        temp.Snokta.Z = Poligon[2].z;
                                        YardimciObje.Line.Add(temp);
                                    }

                                    if (gizle[2] == false)
                                    {
                                        temp = new YardimciObje.CizgiO();
                                        temp.Renk = Color.LightYellow;
                                        temp.layer = "0";

                                        temp.Bnokta.X = Poligon[2].x;
                                        temp.Bnokta.Y = Poligon[2].y;
                                        temp.Bnokta.Z = Poligon[2].z;

                                        temp.Snokta.X = Poligon[3].x;
                                        temp.Snokta.Y = Poligon[3].y;
                                        temp.Snokta.Z = Poligon[3].z;
                                        YardimciObje.Line.Add(temp);
                                    }

                                    if (gizle[3] == false)
                                    {
                                        temp = new YardimciObje.CizgiO();
                                        temp.Renk = Color.LightYellow;
                                        temp.layer = "0";

                                        temp.Bnokta.X = Poligon[3].x;
                                        temp.Bnokta.Y = Poligon[3].y;
                                        temp.Bnokta.Z = Poligon[3].z;

                                        temp.Snokta.X = Poligon[0].x;
                                        temp.Snokta.Y = Poligon[0].y;
                                        temp.Snokta.Z = Poligon[0].z;
                                        YardimciObje.Line.Add(temp);
                                    }

                                    //1 2 4 8  Görünmeyenler

                                
                            }
                        }


                        if (buf == "LINE")
                        {

                            YardimciObje.CizgiO temp = new YardimciObje.CizgiO();
                            temp.Renk = Color.LightYellow;


                            bool breakflag = false;

                            {
                                for (int x = 0; x < 8; x++) //8 kez okuma
                                    buf = sr.ReadLine();
                            }

                            // if ((buf.Trim()).ToLower() == "kes")
                            temp.layer = buf.Trim().ToLower(); //kes layeri


                            {
                                while (!breakflag)
                                {
                                    buf = sr.ReadLine();

                                    if (buf.Trim() == "10")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Bnokta.X = float.Parse(buf);
                                    }
                                    else if (buf.Trim() == "11")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Snokta.X = float.Parse(buf);
                                    }
                                  

                                    else if (buf.Trim() == "20")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Bnokta.Y = float.Parse(buf);
                                    }
                                    else if (buf.Trim() == "21")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Snokta.Y = float.Parse(buf);
                                    }

                                    else if (buf.Trim() == "30")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Bnokta.Z = float.Parse(buf);
                                    }
                                    else if (buf.Trim() == "31")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Snokta.Z = float.Parse(buf);

                                        breakflag = true;
                                    }
                                  

                                    //if (breakflag) break;
                                }
                                YardimciObje.Line.Add(temp);
                            }

                        }

                    }

                    fs.Close();

                }

                ObjeCevir();
                // this.Refresh();
                
                
                for (int x = 0; x < YardimciObje.Line.Count ; x++)
                {

                    YardimciObje.CizgiO temL = new YardimciObje.CizgiO();

                   temL = YardimciObje.Line[x];
                    temL.Bnokta.X = Math.Truncate(temL.Bnokta.X * 1000) / 1000;
                    temL.Bnokta.Y = Math.Truncate(temL.Bnokta.Y * 1000) / 1000;
                    temL.Bnokta.Z = Math.Truncate(temL.Bnokta.Z * 1000) / 1000;
                                                                     
                    temL.Snokta.X = Math.Truncate(temL.Snokta.X * 1000) / 1000;
                    temL.Snokta.Y = Math.Truncate(temL.Snokta.Y * 1000) / 1000;
                    temL.Snokta.Z = Math.Truncate(temL.Snokta.Z * 1000) / 1000;

                    YardimciObje.Line[x] = temL;
              

                    if (x == 0)
                    {
                        maxX = YardimciObje.Line[x].Bnokta.X;
                        maxY = YardimciObje.Line[x].Bnokta.Y;
                        maxZ = YardimciObje.Line[x].Bnokta.Z;
                        minX = YardimciObje.Line[x].Snokta.X;
                        minY = YardimciObje.Line[x].Snokta.Y;
                        minZ = YardimciObje.Line[x].Snokta.Z;
                    }

                    if (maxX < YardimciObje.Line[x].Bnokta.X) maxX = YardimciObje.Line[x].Bnokta.X;
                    if (maxX < YardimciObje.Line[x].Snokta.X) maxX = YardimciObje.Line[x].Snokta.X;

                    if (maxY < YardimciObje.Line[x].Bnokta.Y) maxY = YardimciObje.Line[x].Bnokta.Y;
                    if (maxY < YardimciObje.Line[x].Snokta.Y) maxY = YardimciObje.Line[x].Snokta.Y;

                    if (maxZ < YardimciObje.Line[x].Bnokta.Z) maxZ = YardimciObje.Line[x].Bnokta.Z;
                    if (maxZ < YardimciObje.Line[x].Snokta.Z) maxZ = YardimciObje.Line[x].Snokta.Z;

                    if (minX > YardimciObje.Line[x].Bnokta.X) minX = YardimciObje.Line[x].Bnokta.X;
                    if (minX > YardimciObje.Line[x].Snokta.X) minX = YardimciObje.Line[x].Snokta.X;
                                                           
                    if (minY > YardimciObje.Line[x].Bnokta.Y) minY = YardimciObje.Line[x].Bnokta.Y;
                    if (minY > YardimciObje.Line[x].Snokta.Y) minY = YardimciObje.Line[x].Snokta.Y;
                                                            
                    if (minZ > YardimciObje.Line[x].Bnokta.Z) minZ = YardimciObje.Line[x].Bnokta.Z;
                    if (minZ > YardimciObje.Line[x].Snokta.Z) minZ = YardimciObje.Line[x].Snokta.Z;

                }

                
                yOrt = (maxY - minY) / 2;
                zOrt = (maxZ - minZ) / 2;

                //if (Math.Abs(maxX) < Math.Abs(minX)) maxX = minX;
                //if (Math.Abs(maxY) < Math.Abs(minY)) maxY = minY;
                //if (Math.Abs(maxZ) < Math.Abs(minZ)) maxZ = minZ;
               
                maxX = maxX - minX;
                double FarkZ = maxZ - (maxZ - minZ) / 2;
                double FarkY = maxY - (maxY - minY) / 2;

                double minx = minX;
                double minC = zOrt;
                for (int x = 0; x < YardimciObje.Line.Count; x++)
                {
                    YardimciObje.CizgiO temp = new YardimciObje.CizgiO();
                    temp = YardimciObje.Line[x];
                   temp.gizli = true;
                    if (x == 0)
                    {
                        DisCap = zOrt;
                        icCap = DisCap;
                    }
                    temp.Bnokta.X = temp.Bnokta.X - minX;
                    temp.Bnokta.Y = temp.Bnokta.Y -   FarkY;
                    temp.Bnokta.Z = temp.Bnokta.Z  - FarkZ;

                    temp.Snokta.X = temp.Snokta.X - minX;
                    temp.Snokta.Y = temp.Snokta.Y -   FarkY;
                    temp.Snokta.Z = temp.Snokta.Z   - FarkZ;
                    /*
                    if (Math.Abs(temp.Bnokta.Y) <= 0.01 && Math.Abs( temp.Bnokta.Z) > 0)
                        minx = Math.Sqrt(Math.Pow(temp.Bnokta.Z, 2));

                    if (Math.Abs(temp.Snokta.Y) <= 0.01 && Math.Abs(temp.Bnokta.Z )> 0)
                        minC = Math.Sqrt(Math.Pow(temp.Snokta.Z, 2));

                 
                    if (minx > minC) minx = minC;

                    if (x == 0) icCap = minx;

                    if (minx < icCap) icCap = minx;
                    */

                    double Dis_R = Math.Sqrt(Math.Pow(temp.Bnokta.Y, 2) + Math.Pow(temp.Bnokta.Z, 2));
                    double Dis_Rx = Math.Sqrt(Math.Pow(temp.Snokta.Y, 2) + Math.Pow(temp.Snokta.Z, 2));
                    if (Dis_R < Dis_Rx) Dis_R = Dis_Rx;

                    double Ic_R = Math.Sqrt(Math.Pow(temp.Snokta.Y, 2) + Math.Pow(temp.Snokta.Z, 2));
                    double Ic_Rx = Math.Sqrt(Math.Pow(temp.Bnokta.Y, 2) + Math.Pow(temp.Bnokta.Z, 2));
                    if (Ic_R > Ic_Rx) 
                        Ic_R = Ic_Rx;

                    if (Dis_R > DisCap) DisCap = Dis_R;
                    if (Ic_R < icCap) icCap = Ic_R;

                    YardimciObje.Line[x] = temp;
                }


                 icCap = Math.Round(icCap * 2,2);
                 DisCap =Math.Round(DisCap * 2,2);
                kalinlik = Math.Round((DisCap - icCap) / 2,2); //

               


                D_Cap.Text = DisCap.ToString();
                B_kalinlik.Text = kalinlik.ToString();

          
            }
            catch (Exception ex)
            {
                //this.Refresh();
                CizimG2D.GrafikCiz();
            }
            CizimG2D.GrafikCiz();
        }
        private void UcBulma()
        {Bas:;
            for (int x = 0; x < YardimciObje.Line.Count; x++)
            {
                for (int y = x; y < YardimciObje.Line.Count; y++)
                {

                    if (x == 7 && y == 213)
                    {
                        double k = 0;
                    }

                    if (x == y) continue;

                    //Cubuk uzunluğu sıfır olan
                    if(               Math.Abs(YardimciObje.Line[x].Bnokta.X - YardimciObje.Line[x].Snokta.X) < 0.01 &&
                                      Math.Abs(YardimciObje.Line[x].Bnokta.Y - YardimciObje.Line[x].Snokta.Y) < 0.01 &&
                                      Math.Abs(YardimciObje.Line[x].Bnokta.Z - YardimciObje.Line[x].Snokta.Z) < 0.01  )
                    {
                        YardimciObje.Line.RemoveAt(x);
                        goto Bas;
                    }


                    if (Math.Abs(YardimciObje.Line[x].Bnokta.X - YardimciObje.Line[y].Bnokta.X) < 0.01 &&
                     Math.Abs(YardimciObje.Line[x].Bnokta.Y - YardimciObje.Line[y].Bnokta.Y) < 0.01 &&
                     Math.Abs(YardimciObje.Line[x].Bnokta.Z - YardimciObje.Line[y].Bnokta.Z) < 0.01 &&
                     Math.Abs(YardimciObje.Line[x].Snokta.X - YardimciObje.Line[y].Snokta.X) < 0.01 &&
                     Math.Abs(YardimciObje.Line[x].Snokta.Y - YardimciObje.Line[y].Snokta.Y) < 0.01 &&
                     Math.Abs(YardimciObje.Line[x].Snokta.Z - YardimciObje.Line[y].Snokta.Z) < 0.01  )
                    {
                        YardimciObje.Line.RemoveAt(y);
                         goto Bas;
                    }


                    if (Math.Abs(YardimciObje.Line[x].Bnokta.X - YardimciObje.Line[y].Snokta.X) < 0.01 &&
                        Math.Abs(YardimciObje.Line[x].Bnokta.Y - YardimciObje.Line[y].Snokta.Y) < 0.01 &&
                        Math.Abs(YardimciObje.Line[x].Bnokta.Z - YardimciObje.Line[y].Snokta.Z) < 0.01 &&
                        Math.Abs(YardimciObje.Line[x].Snokta.X - YardimciObje.Line[y].Bnokta.X) < 0.01 &&
                        Math.Abs(YardimciObje.Line[x].Snokta.Y - YardimciObje.Line[y].Bnokta.Y) < 0.01 &&
                        Math.Abs(YardimciObje.Line[x].Snokta.Z - YardimciObje.Line[y].Bnokta.Z) < 0.01)
                    {
                        YardimciObje.Line.RemoveAt(y);
                         goto Bas;
                    }


                }

                }



                for (int x = 0; x < YardimciObje.Line.Count; x++)
            {
                if (x == 0)
                {
                    maxX = YardimciObje.Line[x].Bnokta.X;
                    maxY = YardimciObje.Line[x].Bnokta.Y;
                    maxZ = YardimciObje.Line[x].Bnokta.Z;
                    minX = YardimciObje.Line[x].Snokta.X;
                    minY = YardimciObje.Line[x].Snokta.Y;
                    minZ = YardimciObje.Line[x].Snokta.Z;
                }

                if (maxX < YardimciObje.Line[x].Bnokta.X) maxX = YardimciObje.Line[x].Bnokta.X;
                if (maxX < YardimciObje.Line[x].Snokta.X) maxX = YardimciObje.Line[x].Snokta.X;

                if (maxY < YardimciObje.Line[x].Bnokta.Y) maxY = YardimciObje.Line[x].Bnokta.Y;
                if (maxY < YardimciObje.Line[x].Snokta.Y) maxY = YardimciObje.Line[x].Snokta.Y;

                if (maxZ < YardimciObje.Line[x].Bnokta.Z) maxZ = YardimciObje.Line[x].Bnokta.Z;
                if (maxZ < YardimciObje.Line[x].Snokta.Z) maxZ = YardimciObje.Line[x].Snokta.Z;

                if (minX > YardimciObje.Line[x].Bnokta.X) minX = YardimciObje.Line[x].Bnokta.X;
                if (minX > YardimciObje.Line[x].Snokta.X) minX = YardimciObje.Line[x].Snokta.X;

                if (minY > YardimciObje.Line[x].Bnokta.Y) minY = YardimciObje.Line[x].Bnokta.Y;
                if (minY > YardimciObje.Line[x].Snokta.Y) minY = YardimciObje.Line[x].Snokta.Y;

                if (minZ > YardimciObje.Line[x].Bnokta.Z) minZ = YardimciObje.Line[x].Bnokta.Z;
                if (minZ > YardimciObje.Line[x].Snokta.Z) minZ = YardimciObje.Line[x].Snokta.Z;

            }

            yOrt = (maxY - minY) / 2;
            zOrt = (maxZ - minZ) / 2;

            if (Math.Abs(maxX) < Math.Abs(minX)) maxX = minX;
            if (Math.Abs(maxY) < Math.Abs(minY)) maxY = minY;
            if (Math.Abs(maxZ) < Math.Abs(minZ)) maxZ = minZ;

           


        }



        private void UclaraNotaVer()
        {
            int No = 0;
            YardimciObje.Nokta.Clear();

            for (int x = 0; x < YardimciObje.Line.Count; x++)
            {
                YardimciObje.CizgiO temp = new YardimciObje.CizgiO();
                temp = YardimciObje.Line[x];
                temp.gizli = false;

                double ilk = Math.Sqrt(Math.Pow(temp.Bnokta.Y, 2) + Math.Pow(temp.Bnokta.Z, 2));
                double son = Math.Sqrt(Math.Pow(temp.Snokta.Y, 2) + Math.Pow(temp.Snokta.Z, 2));

                double icR = zOrt - kalinlik - 1;
                double disR = zOrt + 1;



                if ((temp.Bnokta.X < UcMesafe || temp.Snokta.X > maxX - UcMesafe) ||
                       (temp.Snokta.X < UcMesafe || temp.Bnokta.X > maxX - UcMesafe))

                {
                    if (Math.Abs(temp.Bnokta.Y - temp.Snokta.Y) <= 0.01) temp.gizli = true;
                    if (Math.Abs(temp.Bnokta.Z - temp.Snokta.Z) <= 0.01) temp.gizli = true;
                }


                if ((temp.Bnokta.X <= UcMesafe && temp.Snokta.X <= UcMesafe) ||
                       (temp.Snokta.X >= maxX - UcMesafe && temp.Bnokta.X >= maxX - UcMesafe))

                {
                    if ((Math.Abs(temp.Bnokta.Y - temp.Snokta.Y) <= 0.01) && (Math.Abs(temp.Bnokta.Z - temp.Snokta.Z) <= 0.01)) temp.gizli = false;

                }


                double DRust = DisCap / 2 + kalinlik / 2;
                double DRalt = DisCap / 2 - kalinlik / 2;

                double IRust = icCap / 2 + kalinlik / 2;
                double IRalt = icCap / 2 - kalinlik / 2;


                if ((ilk <= DRust && ilk >= DRalt &&
                    (son <= IRust && son >= IRalt)) ||
                    (son <= DRust && son >= DRalt &&
                    (ilk <= IRust && ilk >= IRalt)))
                {
                    temp.gizli = true;

                }

                if (temp.gizli == false)
                {

                    if (ilk > DRalt && son > DRalt)
                    {
                        temp.dis = false;
                        #region Nokta bul
                        YardimciObje.NoktaO tempNok = new YardimciObje.NoktaO();
                        YardimciObje.NoktaO tempNokS = new YardimciObje.NoktaO();
                        tempNok.dis = false;
                        tempNok.PointXYZ.X = temp.Bnokta.X;
                        tempNok.PointXYZ.Y = temp.Bnokta.Y;
                        tempNok.PointXYZ.Z = temp.Bnokta.Z;


                        tempNokS.dis = false;
                        tempNokS.PointXYZ.X = temp.Snokta.X;
                        tempNokS.PointXYZ.Y = temp.Snokta.Y;
                        tempNokS.PointXYZ.Z = temp.Snokta.Z;

                        int i = YardimciObje.Nokta.FindIndex(c => Math.Abs(c.PointXYZ.X - tempNok.PointXYZ.X) < 0.01 && Math.Abs(c.PointXYZ.Y - tempNok.PointXYZ.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - tempNok.PointXYZ.Z) < 0.01);
                        if (i < 0)
                        {
                            No = No + 1;
                            tempNok.No = No;
                            YardimciObje.Nokta.Add(tempNok);
                            temp.BN = No;

                        }
                        else
                        {
                            temp.BN = YardimciObje.Nokta[i].No;

                        }

                        int j = YardimciObje.Nokta.FindIndex(c => Math.Abs(c.PointXYZ.X - tempNokS.PointXYZ.X) < 0.01 && Math.Abs(c.PointXYZ.Y - tempNokS.PointXYZ.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - tempNokS.PointXYZ.Z) < 0.01);
                        if (j < 0)
                        {
                            No = No + 1;
                            tempNokS.No = No;
                            YardimciObje.Nokta.Add(tempNokS);
                            temp.SN = No;

                        }
                        else
                        {
                            temp.SN = YardimciObje.Nokta[j].No;

                        }
                        #endregion
                    }
                    else
                    {
                        temp.dis = true;
                        #region Nokta bul
                        YardimciObje.NoktaO tempNok = new YardimciObje.NoktaO();
                        YardimciObje.NoktaO tempNokS = new YardimciObje.NoktaO();
                        tempNok.dis = true;
                        tempNok.PointXYZ.X = temp.Bnokta.X;
                        tempNok.PointXYZ.Y = temp.Bnokta.Y;
                        tempNok.PointXYZ.Z = temp.Bnokta.Z;


                        tempNokS.dis = true;
                        tempNokS.PointXYZ.X = temp.Snokta.X;
                        tempNokS.PointXYZ.Y = temp.Snokta.Y;
                        tempNokS.PointXYZ.Z = temp.Snokta.Z;

                        int i = YardimciObje.Nokta.FindIndex(c => Math.Abs(c.PointXYZ.X - tempNok.PointXYZ.X) < 0.01 && Math.Abs(c.PointXYZ.Y - tempNok.PointXYZ.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - tempNok.PointXYZ.Z) < 0.01);
                        if (i < 0)
                        {
                            No = No + 1;
                            tempNok.No = No;
                            YardimciObje.Nokta.Add(tempNok);
                            temp.BN = No;

                        }
                        else
                        {
                            temp.BN = YardimciObje.Nokta[i].No;

                        }

                        int j = YardimciObje.Nokta.FindIndex(c => Math.Abs(c.PointXYZ.X - tempNokS.PointXYZ.X) < 0.01 && Math.Abs(c.PointXYZ.Y - tempNokS.PointXYZ.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - tempNokS.PointXYZ.Z) < 0.01);
                        if (j < 0)
                        {
                            No = No + 1;
                            tempNokS.No = No;
                            YardimciObje.Nokta.Add(tempNokS);
                            temp.SN = No;

                        }
                        else
                        {
                            temp.SN = YardimciObje.Nokta[j].No;

                        }
                        #endregion

                    }
                }



                YardimciObje.Line[x] = temp;
            }

        }

        private void DXFAc()
        { 

            try
            {
                openFileDialog1.ShowDialog();
                if (openFileDialog1.FileName != "")
                {
                    AnaPencere.ActiveForm.Text = "Boru Gcode Oluşturma  " + openFileDialog1.FileName;
                    YardimciObje.LineGcode.Clear();
                    YardimciObje.Kesim.Clear();
                    YardimciObje.Line.Clear();
                    YardimciObje.Daire.Clear();
                    YardimciObje.Arc.Clear();
                    YardimciObje.Pline.Clear();
                    YardimciObje.AnimateLine.Clear();
                    listBox1.Items.Clear();

                    FileStream fs = new System.IO.FileStream(
                        openFileDialog1.FileName,
                        System.IO.FileMode.Open,
                        System.IO.FileAccess.Read);

                    StreamReader sr = new StreamReader(fs);

                    while (sr.Peek() != -1)
                    {
                        string buf = sr.ReadLine();
                        Console.WriteLine(buf);
                        if (buf == "LINE")
                        {

                            YardimciObje.CizgiO temp = new YardimciObje.CizgiO();
                            temp.Renk = Color.LightYellow;


                            bool breakflag = false;

                            {
                                for (int x = 0; x < 8; x++) //8 kez okuma
                                    buf = sr.ReadLine();
                            }

                            // if ((buf.Trim()).ToLower() == "kes")
                            temp.layer = buf.Trim().ToLower(); //kes layeri


                            {
                                while (!breakflag)
                                {
                                    buf = sr.ReadLine();

                                    if (buf.Trim() == "10")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Bnokta.X = float.Parse(buf);
                                    }
                                    else if (buf.Trim() == "11")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Snokta.X = float.Parse(buf);
                                    }
                                    else if (buf.Trim() == "20")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Bnokta.Y = float.Parse(buf);
                                    }
                                    else if (buf.Trim() == "21")
                                    {
                                        buf = sr.ReadLine();
                                        temp.Snokta.Y = float.Parse(buf);

                                        breakflag = true;
                                    }

                                    if (breakflag) break;
                                }
                                YardimciObje.Line.Add(temp);
                            }
 
                        }
                        else if (buf == "CIRCLE")
                        {

                            YardimciObje.DaireO temp = new YardimciObje.DaireO();

                            bool breakflag = false;
                            while (!breakflag)
                            {
                                buf = sr.ReadLine();
                                if (buf.Trim() == "10")
                                {
                                    buf = sr.ReadLine();
                                    temp.Mnokta.X = float.Parse(buf);
                                }
                                else if (buf.Trim() == "20")
                                {
                                    buf = sr.ReadLine();
                                    temp.Mnokta.Y = float.Parse(buf);
                                }
                                else if (buf.Trim() == "40")
                                {
                                    buf = sr.ReadLine();
                                    temp.YariCap = float.Parse(buf);
                                    breakflag = true;
                                }

                                if (breakflag) break;
                            }
                            YardimciObje.Daire.Add(temp);
                        }
                        else if (buf == "ARC")
                        {

                            YardimciObje.ArcO temp = new YardimciObje.ArcO();

                            bool breakflag = false;
                            while (!breakflag)
                            {
                                buf = sr.ReadLine();
                                if (buf.Trim() == "10")
                                {
                                    buf = sr.ReadLine();
                                    temp.Mnokta.X = float.Parse(buf);
                                }
                                else if (buf.Trim() == "20")
                                {
                                    buf = sr.ReadLine();
                                    temp.Mnokta.Y = float.Parse(buf);
                                }
                                /* else if (buf.Trim() == "30")
                                 {
                                     buf = sr.ReadLine();
                                     temp.Mnokta.Z = AktifKat.KatKot;
                                 }*/
                                else if (buf.Trim() == "40")
                                {
                                    buf = sr.ReadLine();
                                    temp.YariCap = float.Parse(buf);
                                }

                                else if (buf.Trim() == "50")
                                {
                                    buf = sr.ReadLine();
                                    temp.Aci1 = float.Parse(buf);
                                }
                                else if (buf.Trim() == "51")
                                {
                                    buf = sr.ReadLine();
                                    temp.Aci2 = float.Parse(buf);

                                    breakflag = true;
                                }


                                if (breakflag) break;
                            }
                            YardimciObje.Arc.Add(temp);
                        }

                    }
                    //閉じる
                    fs.Close();


                }
                this.Refresh();

            }
            catch
            {
                this.Refresh();
            }

            CizimG2D.GrafikCiz();

        }
        private void GcodeYap()
        {
            try
            {
                CizimG2D.Mod = 0;
                KesimAdet = Convert.ToInt32(textBox1.Text);
               
                YardimciObje.Kesim.Clear();
                YardimciObje.AnimateLine.Clear();
             //   goto Basla;
                YardimciObje.LineGcode.Clear();
            Temizle:;

                for (int i = 0; i < YardimciObje.Line.Count; i++)
                {
                    for (int j = i + 1; j < YardimciObje.Line.Count; j++)
                    {

                        double X1 = YardimciObje.Line[i].Bnokta.X;
                        double Y1 = YardimciObje.Line[i].Bnokta.Y;
                        double X2 = YardimciObje.Line[i].Snokta.X;
                        double Y2 = YardimciObje.Line[i].Snokta.Y;

                        double X1m = YardimciObje.Line[j].Bnokta.X;
                        double Y1m = YardimciObje.Line[j].Bnokta.Y;
                        double X2m = YardimciObje.Line[j].Snokta.X;
                        double Y2m = YardimciObje.Line[j].Snokta.Y;
                        if (X1 == X1m && Y1 == Y1m && X2 == X2m && Y2 == Y2m)
                        {
                            if (YardimciObje.Line[i].layer == "kes")
                            { YardimciObje.Line.RemoveAt(j); }
                            else
                            { YardimciObje.Line.RemoveAt(i); }



                            goto Temizle;

                        }


                        if (X1 == X2m && Y1 == Y2m && X2 == X1m && Y2 == Y1m)
                        {
                            if (YardimciObje.Line[i].layer == "kes")
                            { YardimciObje.Line.RemoveAt(j); }
                            else
                            { YardimciObje.Line.RemoveAt(i); }



                            goto Temizle;

                        }



                    }

                }









                for (int i = 0; i < YardimciObje.Line.Count; i++)
                {
                    if (YardimciObje.Line[i].layer == "kes")
                    {
                        YardimciObje.LineGcode.Add(YardimciObje.Line[i]);
                    }
                }

            Basla:;

                double MinX = 0, MinY = 0;
                MinX = YardimciObje.LineGcode[0].Bnokta.X;
                MinY = YardimciObje.LineGcode[0].Bnokta.Y;
                for (int i = 0; i < YardimciObje.LineGcode.Count; i++)
                {
                    if (YardimciObje.LineGcode[i].Bnokta.X < MinX) MinX = YardimciObje.LineGcode[i].Bnokta.X;
                    if (YardimciObje.LineGcode[i].Snokta.X < MinX) MinX = YardimciObje.LineGcode[i].Snokta.X;
                    if (YardimciObje.LineGcode[i].Bnokta.Y < MinY) MinY = YardimciObje.LineGcode[i].Bnokta.Y;
                    if (YardimciObje.LineGcode[i].Snokta.Y < MinY) MinY = YardimciObje.LineGcode[i].Snokta.Y;
                }


                for (int i = 0; i < YardimciObje.LineGcode.Count; i++) //en küçük noktayı sıfıra çek
                {
                    YardimciObje.CizgiO Linex = new YardimciObje.CizgiO();
                    Linex.Bnokta.X = YardimciObje.LineGcode[i].Bnokta.X - MinX;
                    Linex.Bnokta.Y = YardimciObje.LineGcode[i].Bnokta.Y - MinY;
                    Linex.Snokta.X = YardimciObje.LineGcode[i].Snokta.X - MinX;
                    Linex.Snokta.Y = YardimciObje.LineGcode[i].Snokta.Y - MinY;
                    Linex.layer = YardimciObje.LineGcode[i].layer;
                    YardimciObje.LineGcode[i] = Linex;
                }

                for (int i = 0; i < YardimciObje.Line.Count; i++) //en küçük noktayı sıfıra çek
                {
                    YardimciObje.CizgiO Linex = new YardimciObje.CizgiO();
                    Linex.Bnokta.X = YardimciObje.Line[i].Bnokta.X - MinX;
                    Linex.Bnokta.Y = YardimciObje.Line[i].Bnokta.Y - MinY;
                    Linex.Snokta.X = YardimciObje.Line[i].Snokta.X - MinX;
                    Linex.Snokta.Y = YardimciObje.Line[i].Snokta.Y - MinY;
                    Linex.layer = YardimciObje.Line[i].layer;
                    YardimciObje.Line[i] = Linex;
                }


                // ilk nokta
                // Y=0 olan ve x en küçük olan
                double ilkNoktaX = YardimciObje.LineGcode[0].Bnokta.X;
                double ilkNoktaY = YardimciObje.LineGcode[0].Bnokta.Y;
                int baslangic = 0;
                List<int> ListeNo = new List<int>();

                for (int i = 0; i < YardimciObje.LineGcode.Count; i++) //en küçük noktayı sıfıra çek
                {

                    if (YardimciObje.LineGcode[i].Bnokta.Y == 0 && YardimciObje.LineGcode[i].Bnokta.X < ilkNoktaX)
                    {
                        ilkNoktaX = YardimciObje.LineGcode[i].Bnokta.X;
                        ilkNoktaY = YardimciObje.LineGcode[i].Bnokta.Y;
                        baslangic = i;
                    }
                    ListeNo.Add(i);
                    //Console.WriteLine("Sira={2} Xb={0} Yb={1} Xs={3} Ys={4}", YardimciObje.LineGcode[i].Bnokta.X, YardimciObje.LineGcode[i].Bnokta.Y, i,YardimciObje.LineGcode[i].Snokta.X, YardimciObje.LineGcode[i].Snokta.Y);
                }
                // Console.WriteLine("X={0} Y={1}", ilkNoktaX, ilkNoktaY);

                int Temp = baslangic;
                ListeNo.Remove(Temp);
                int kontrol = 0;
            Bas2:;

                List<int> Kesx = new List<int>();
                Kesx.Add(Temp);
                ListeNo.Remove(Temp);
            Bas:;
                kontrol = 1;
            Bas3:;
                if (ListeNo.Count == 0)
                {
                    YardimciObje.Kesim.Add(Kesx);
                }


                for (int i = 0; i < ListeNo.Count; i++) //en küçük noktayı sıfıra çek
                {

                    int sonuc1 = NoktaAra(Kesx, ListeNo, YardimciObje.LineGcode[Temp].Bnokta.X, YardimciObje.LineGcode[Temp].Bnokta.Y);
                    if (sonuc1 > 0)
                    {
                        Temp = sonuc1;
                        kontrol = 0;
                        goto Bas;
                    }
                    int sonuc2 = NoktaAra(Kesx, ListeNo, YardimciObje.LineGcode[Temp].Snokta.X, YardimciObje.LineGcode[Temp].Snokta.Y);
                    if (sonuc2 > 0)
                    {
                        Temp = sonuc2;
                        kontrol = 0;
                        goto Bas;
                    }
                    if (kontrol == 1)
                    {
                        Temp = baslangic;
                        kontrol = 2;
                        goto Bas3;
                    }

                    /// ilk bas

                    {
                        if (ListeNo.Count > 0)
                        {
                            Temp = ListeNo[0];
                            baslangic = Temp;
                            //Kesx.Add(Temp);
                            YardimciObje.Kesim.Add(Kesx);
                            ListeNo.RemoveAt(0);
                            goto Bas2;
                        }
                    }

                }
                Sirala();
            }
            catch
            { }

        }
        private void button2_Click(object sender, EventArgs e)
        {
            GcodeYap();
        }
        private void Sirala()
        {

           /* for (int j = 0; j < YardimciObje.Kesim[0].Count; j++)
            {
                int nokta = YardimciObje.Kesim[0][j];
                Console.WriteLine("ilk-No= {5} -Sira={2} Xb={0} Yb={1} Xs={3} Ys={4}", YardimciObje.LineGcode[nokta].Bnokta.X, YardimciObje.LineGcode[nokta].Bnokta.Y, YardimciObje.Kesim[0][j], YardimciObje.LineGcode[nokta].Snokta.X, YardimciObje.LineGcode[nokta].Snokta.Y,j);

            }
            Console.WriteLine("---------------------------------------------");*/

            for (int i = 0; i < YardimciObje.Kesim.Count; i++)
            {
                YardimciObje.Kesim[i]= BaslangicBul(YardimciObje.Kesim[i]);
                YardimciObje.Kesim[i] = Duzenle(YardimciObje.Kesim[i]);
            }

           for (int i = 0; i < YardimciObje.Kesim.Count; i++)
            {
                YardimciObje.Kesim[i] = Duzenle(YardimciObje.Kesim[i]);
            }


            siralaKucuk.Clear();
           
            maxx = 0;
            maxy = 0;
            for (int i = 0; i < YardimciObje.Kesim.Count; i++)
            {
                YardimciObje.siralamao siralamax = new YardimciObje.siralamao();
                minx = 9999999999999;
                for (int j = 0; j < YardimciObje.Kesim[i].Count; j++)
                {
                     
                    int nokta = YardimciObje.Kesim[i][j];
                    if (minx > YardimciObje.LineGcode[nokta].Bnokta.X) minx = YardimciObje.LineGcode[nokta].Bnokta.X;
                    if (minx > YardimciObje.LineGcode[nokta].Snokta.X) minx = YardimciObje.LineGcode[nokta].Snokta.X;

                    if (maxx < YardimciObje.LineGcode[nokta].Bnokta.X) maxx = YardimciObje.LineGcode[nokta].Bnokta.X;
                    if (maxx < YardimciObje.LineGcode[nokta].Snokta.X) maxx = YardimciObje.LineGcode[nokta].Snokta.X;

                    if (maxy< YardimciObje.LineGcode[nokta].Bnokta.Y) maxy = YardimciObje.LineGcode[nokta].Bnokta.Y;
                    if (maxy < YardimciObje.LineGcode[nokta].Snokta.Y) maxy = YardimciObje.LineGcode[nokta].Snokta.Y;
                }
               
                siralamax.index = i;
                siralamax.xboy = minx;
                siralaKucuk.Add(siralamax);
            }
            cap =Math.Round( maxy / (Math.PI),2);

            GFG gg = new GFG();
            siralaKucuk.Sort(gg);

            List<List<int>> TempSira = new List<List<int>>();

            for (int i = 0; i < siralaKucuk.Count; i++)
            {
                TempSira.Add(YardimciObje.Kesim[siralaKucuk[i].index]);
            }

            YardimciObje.Kesim = TempSira;


            for (int i = 0; i < YardimciObje.Kesim.Count; i++)
            {
                Console.WriteLine("---------------------------------------------------");
                for (int j = 0; j < YardimciObje.Kesim[i].Count; j++)
                {
                    int nokta = YardimciObje.Kesim[i][j];
                    Console.WriteLine("No= {5} Sira={2} Xb={0} Yb={1} Xs={3} Ys={4}", YardimciObje.LineGcode[nokta].Bnokta.X, YardimciObje.LineGcode[nokta].Bnokta.Y, YardimciObje.Kesim[i][j], YardimciObje.LineGcode[nokta].Snokta.X, YardimciObje.LineGcode[nokta].Snokta.Y, j);
                }
            }
            Gcodeolustur();
        }
        public List<int> TerseCevir(List<int> TerseCevirList)
        {


            TerseCevirList.Reverse();

            for (int i = 0; i < TerseCevirList.Count; i++)
            {
                YardimciObje.CizgiO Temp = new YardimciObje.CizgiO();
 
                Temp.Bnokta.X = YardimciObje.LineGcode[TerseCevirList[i]].Snokta.X;
                Temp.Bnokta.Y = YardimciObje.LineGcode[TerseCevirList[i]].Snokta.Y;
                Temp.Bnokta.Z = YardimciObje.LineGcode[TerseCevirList[i]].Snokta.Z;

                Temp.Snokta.X = YardimciObje.LineGcode[TerseCevirList[i]].Bnokta.X;
                Temp.Snokta.Y = YardimciObje.LineGcode[TerseCevirList[i]].Bnokta.Y;
                Temp.Snokta.Z = YardimciObje.LineGcode[TerseCevirList[i]].Bnokta.Z;
                Temp.layer = YardimciObje.LineGcode[TerseCevirList[i]].layer;

                Temp.Renk = YardimciObje.LineGcode[TerseCevirList[i]].Renk;

                YardimciObje.LineGcode[TerseCevirList[i]] = Temp;

            }
            
            return TerseCevirList;





        }
        public void Gcodeolustur()
        {
            double BrAci = 400 / maxy;
            YardimciObje.GcodeString.Clear();
            YardimciObje.GcodeString.Add(Math.Round(maxx,2).ToString());//Boru uzunlugu
            YardimciObje.GcodeString.Add(cap.ToString());//Boru Capi
            YardimciObje.GcodeString.Add(KesimAdet.ToString());//Boru kesim Adedi
            YardimciObje.GcodeString.Add("G6");//Plazma kapalı
            YardimciObje.GcodeString.Add("G92");// X=0 Y=0 Z=0 Kordinat sistemi sıfırla

            double Sonx=0;
            double Sony = 0;
            int mm = 0;
            for (int i = 0; i < YardimciObje.Kesim.Count; i++)
            {
                mm = 0;
            reverse:;
                for (int j = 0; j < YardimciObje.Kesim[i].Count; j++)
                {
                   
                    if (j == 0 && mm==0 )
                    {
                        int r1 = YardimciObje.Kesim[i][j];
                        int r2= YardimciObje.Kesim[i][YardimciObje.Kesim[i].Count-1];
                        double aci1 = Math.Round(YardimciObje.LineGcode[r1].Bnokta.Y * BrAci - Sony * BrAci, 3);
                        double aci2 = Math.Round(YardimciObje.LineGcode[r2].Bnokta.Y * BrAci - Sony * BrAci, 3);
                        double aci3 = Math.Round(YardimciObje.LineGcode[r2].Snokta.Y * BrAci - Sony * BrAci, 3);
                        if (YardimciObje.Kesim[i].Count == 1)
                        {
                            if (Math.Abs(aci1) > Math.Abs(aci3))
                            {

                                YardimciObje.Kesim[i] = TerseCevir(YardimciObje.Kesim[i]);
                                mm = 1;
                                goto reverse;
                            }
                        }
                        else
                        {
                            if (Math.Abs(aci1) > Math.Abs(aci2))
                            {

                                YardimciObje.Kesim[i] = TerseCevir(YardimciObje.Kesim[i]);
                                mm = 1;
                                goto reverse;
                            }
                        }

                      
                        
                    }



               


                        int n1 = YardimciObje.Kesim[i][j];

                    if (j == 0 && mm==0)
                    {
                        double uzunluk = Math.Round(YardimciObje.LineGcode[n1].Bnokta.X - Sonx, 3); ;
                        double acifark = Math.Round(YardimciObje.LineGcode[n1].Bnokta.Y * BrAci - Sony * BrAci, 3);
                        //YardimciObje.GcodeString.Add("G0 " + "X" + uzunluk.ToString() + " E" + acifark.ToString());

                        YardimciObje.GcodeString.Add("G0 " + "X" + uzunluk.ToString());
                        if (acifark != 0)
                        {
                            YardimciObje.GcodeString.Add("G0 E" + acifark.ToString());
                        }





                        YardimciObje.GcodeString.Add("G5");//Plazmayı Aç
                    }


                    if (j > 0 && mm==0)
                    {
                        double u2 = Math.Round(-Sonx + YardimciObje.LineGcode[n1].Bnokta.X, 3);
                        double ac2 = Math.Round(-Sony * BrAci + YardimciObje.LineGcode[n1].Bnokta.Y * BrAci, 3);
                        if (u2 != 0 || ac2 != 0)
                        {
                            YardimciObje.GcodeString.Add("G0 " + "X" + u2.ToString() + " E" + ac2.ToString());
                        }
                    }

                    if (mm == 0)
                    {


                        double u1 = Math.Round(YardimciObje.LineGcode[n1].Snokta.X - YardimciObje.LineGcode[n1].Bnokta.X, 3);
                        double ac1 = Math.Round(YardimciObje.LineGcode[n1].Snokta.Y * BrAci - YardimciObje.LineGcode[n1].Bnokta.Y * BrAci, 3);
                        YardimciObje.GcodeString.Add("G0 " + "X" + u1.ToString() + " E" + ac1.ToString());

                        Sonx = YardimciObje.LineGcode[n1].Snokta.X;
                        Sony = YardimciObje.LineGcode[n1].Snokta.Y;
                    }

                     
                    if (j == 0 && mm == 1)
                 {
                     double uzunluk = Math.Round(YardimciObje.LineGcode[n1].Bnokta.X - Sonx, 3); ;
                     double acifark = Math.Round(YardimciObje.LineGcode[n1].Bnokta.Y * BrAci - Sony * BrAci, 3);

                    //YardimciObje.GcodeString.Add("G0 " + "X" + uzunluk.ToString() + " E" + acifark.ToString());

                       YardimciObje.GcodeString.Add("G0 " + "X" + uzunluk.ToString());
                        if (acifark != 0)
                        {
                            YardimciObje.GcodeString.Add("G0 E" + acifark.ToString());
                        }

                        YardimciObje.GcodeString.Add("G5");//Plazmayı Aç
                 }

                 if (j > 0 && mm == 1)
                 {
                     double u2 = Math.Round(-Sonx + YardimciObje.LineGcode[n1].Bnokta.X, 3);
                     double ac2 = Math.Round(-Sony * BrAci + YardimciObje.LineGcode[n1].Bnokta.Y * BrAci, 3);


                     if (u2 != 0 || ac2 != 0)
                     {
                         YardimciObje.GcodeString.Add("G0 " + "X" + u2.ToString() + " E" + ac2.ToString());
                     }
                 }

                    if (mm == 1)
                    {


                        double u1 = Math.Round(YardimciObje.LineGcode[n1].Snokta.X - YardimciObje.LineGcode[n1].Bnokta.X, 3);
                        double ac1 = Math.Round(YardimciObje.LineGcode[n1].Snokta.Y * BrAci - YardimciObje.LineGcode[n1].Bnokta.Y * BrAci, 3);
                        YardimciObje.GcodeString.Add("G0 " + "X" + u1.ToString() + " E" + ac1.ToString());

                        Sonx = YardimciObje.LineGcode[n1].Snokta.X;
                        Sony = YardimciObje.LineGcode[n1].Snokta.Y;
                    }


                  



                }
                YardimciObje.GcodeString.Add("G6");//Plazma Kapat;
            }

            double u3 = Math.Round(maxx-Sonx+2, 3);
            double ac3 = 0;

            //YardimciObje.GcodeString.Add("G0 " + "X" + u3.ToString() + " E" + ac3.ToString());

            YardimciObje.GcodeString.Add("G0 " + "X" + u3.ToString());
            if (ac3 != 0)
            {
                YardimciObje.GcodeString.Add("G0 E" + ac3.ToString());
            }








            YardimciObje.GcodeString.Add("Basla");//




            listBox1.Items.Clear();
            for (int i = 0; i < YardimciObje.GcodeString.Count; i++)
                listBox1.Items.Add(YardimciObje.GcodeString[i]);

        }
        public List<int> BaslangicBul(List<int> SiralaList)
        {
            int sirabas = -1;
            int n=-1, k=-1;
            for (int i = 0; i < SiralaList.Count; i++)
            {
                int b = 0, s = 0;
                  n = SiralaList[i];


                for (int j = 0; j < SiralaList.Count; j++)
                {
                      k = SiralaList[j];
                    if (i == j) continue;
                    if (Math.Abs(YardimciObje.LineGcode[n].Bnokta.X -YardimciObje.LineGcode[k].Bnokta.X )< 0.0001 && Math.Abs(YardimciObje.LineGcode[n].Bnokta.Y -YardimciObje.LineGcode[k].Bnokta.Y) < 0.0001)
                    {
                        b = 1;
                    }

                    if (Math.Abs(YardimciObje.LineGcode[n].Bnokta.X - YardimciObje.LineGcode[k].Snokta.X )< 0.0001 && Math.Abs(YardimciObje.LineGcode[n].Bnokta.Y - YardimciObje.LineGcode[k].Snokta.Y) < 0.0001)
                    {
                        b = 1;
                    }

                    if (Math.Abs(YardimciObje.LineGcode[n].Snokta.X -YardimciObje.LineGcode[k].Bnokta.X )< 0.0001 && Math.Abs(YardimciObje.LineGcode[n].Snokta.Y -YardimciObje.LineGcode[k].Bnokta.Y) < 0.0001)
                    {
                        s = 1;
                    }

                    if (Math.Abs(YardimciObje.LineGcode[n].Snokta.X - YardimciObje.LineGcode[k].Snokta.X) < 0.0001 && Math.Abs(YardimciObje.LineGcode[n].Snokta.Y -YardimciObje.LineGcode[k].Snokta.Y) < 0.0001)
                    {
                        s = 1;
                    }
                    if (s==1 && b==1) break;

                }
                if ((b == 1 && s == 1)==false)
                {

                    if (sirabas == -1)
                    {
                        sirabas = i;
                        int kl = SiralaList[sirabas];
                        Console.WriteLine("Baslangıc--->>> Sira={2} Xb={0} Yb={1} Xs={3} Ys={4}", YardimciObje.LineGcode[kl].Bnokta.X, YardimciObje.LineGcode[kl].Bnokta.Y, SiralaList[sirabas], YardimciObje.LineGcode[kl].Snokta.X, YardimciObje.LineGcode[kl].Snokta.Y);

                    }
                    else
                    {
                        double ilk = 0,son=0;

                        int o = SiralaList[sirabas];
                        int t = SiralaList[i];
                        if (YardimciObje.LineGcode[o].Bnokta.X < YardimciObje.LineGcode[o].Snokta.X) 
                            ilk = YardimciObje.LineGcode[o].Bnokta.X;
                        else
                            ilk = YardimciObje.LineGcode[o].Snokta.X;

                        if (YardimciObje.LineGcode[t].Bnokta.X < YardimciObje.LineGcode[t].Snokta.X)
                            son = YardimciObje.LineGcode[t].Bnokta.X;
                        else
                            son = YardimciObje.LineGcode[t].Snokta.X;

                        if (ilk > son) sirabas = i;

                    }
                }
            }
            ////////////////////////////////////////
            /// Sıralama
            /// 

            List<int> TempList = new List<int>();

            if (sirabas > -1)
            {
                TempList.Add(SiralaList[sirabas]); ///Basşlangıç elemanı
                SiralaList.RemoveAt(sirabas);
            }
            else
            {
                TempList.Add(SiralaList[0]); // ilk eleman
                SiralaList.RemoveAt(0);
            }


        Bas:;
            int m = TempList[TempList.Count - 1];//Son eklenen
            for (int i = 0; i < SiralaList.Count; i++)
            {
                int p = SiralaList[i];

               

                if (Math.Abs(YardimciObje.LineGcode[p].Bnokta.X - YardimciObje.LineGcode[m].Bnokta.X)<0.0001 && Math.Abs(YardimciObje.LineGcode[p].Bnokta.Y -YardimciObje.LineGcode[m].Bnokta.Y)<0.0001)
                {
                    TempList.Add(SiralaList[i]);
                    SiralaList.RemoveAt(i);
                    goto Bas;
                }

                if (Math.Abs(YardimciObje.LineGcode[p].Bnokta.X - YardimciObje.LineGcode[m].Snokta.X)<0.0001 && Math.Abs(YardimciObje.LineGcode[p].Bnokta.Y - YardimciObje.LineGcode[m].Snokta.Y)<0.0001)
                {
                    TempList.Add(SiralaList[i]);
                    SiralaList.RemoveAt(i);
                    goto Bas;
                }

                if (Math.Abs(YardimciObje.LineGcode[p].Snokta.X - YardimciObje.LineGcode[m].Bnokta.X)<0.00001 && Math.Abs(YardimciObje.LineGcode[p].Snokta.Y - YardimciObje.LineGcode[m].Bnokta.Y)<0.00001)
                {
                    TempList.Add(SiralaList[i]);
                    SiralaList.RemoveAt(i);
                    goto Bas;
                }

                if (Math.Abs(YardimciObje.LineGcode[p].Snokta.X - YardimciObje.LineGcode[m].Snokta.X)<0.00001 && Math.Abs(YardimciObje.LineGcode[p].Snokta.Y - YardimciObje.LineGcode[m].Snokta.Y)<0.0001)
                {
                    TempList.Add(SiralaList[i]);
                    SiralaList.RemoveAt(i);
                    goto Bas;
                }


            }

            return TempList;

        }
        private List<int> Duzenle(List<int> SiralaList)
        {

            for (int i = 0; i < SiralaList.Count-1; i++)
            {

                int c =  SiralaList[i];
                int u =SiralaList[i + 1];

               
                    if (YardimciObje.LineGcode[c].Snokta.X == YardimciObje.LineGcode[u].Snokta.X && YardimciObje.LineGcode[c].Snokta.Y == YardimciObje.LineGcode[u].Snokta.Y)
                    {
                        YardimciObje.CizgiO TempGcode = new YardimciObje.CizgiO();
                        TempGcode = YardimciObje.LineGcode[u];
                        TempGcode.Snokta.X = YardimciObje.LineGcode[u].Bnokta.X;
                        TempGcode.Snokta.Y = YardimciObje.LineGcode[u].Bnokta.Y;
                        TempGcode.Bnokta.X = YardimciObje.LineGcode[u].Snokta.X;
                        TempGcode.Bnokta.Y = YardimciObje.LineGcode[u].Snokta.Y;
                        YardimciObje.LineGcode[u] = TempGcode;
                        continue;
                    }
                

                if (YardimciObje.LineGcode[c].Bnokta.X == YardimciObje.LineGcode[u].Bnokta.X && YardimciObje.LineGcode[c].Bnokta.Y == YardimciObje.LineGcode[u].Bnokta.Y)
                {
                    YardimciObje.CizgiO TempGcode = new YardimciObje.CizgiO();
                    TempGcode = YardimciObje.LineGcode[c];
                    TempGcode.Snokta.X = YardimciObje.LineGcode[c].Bnokta.X;
                    TempGcode.Snokta.Y = YardimciObje.LineGcode[c].Bnokta.Y;
                    TempGcode.Bnokta.X = YardimciObje.LineGcode[c].Snokta.X;
                    TempGcode.Bnokta.Y = YardimciObje.LineGcode[c].Snokta.Y;
                    YardimciObje.LineGcode[c] = TempGcode;
                    continue;
                }

                 
                    if (YardimciObje.LineGcode[c].Bnokta.X == YardimciObje.LineGcode[u].Snokta.X && YardimciObje.LineGcode[c].Bnokta.Y == YardimciObje.LineGcode[u].Snokta.Y)
                    {
                        YardimciObje.CizgiO TempGcode = new YardimciObje.CizgiO();
                        TempGcode = YardimciObje.LineGcode[c];
                        TempGcode.Snokta.X = YardimciObje.LineGcode[c].Bnokta.X;
                        TempGcode.Snokta.Y = YardimciObje.LineGcode[c].Bnokta.Y;
                        TempGcode.Bnokta.X = YardimciObje.LineGcode[c].Snokta.X;
                        TempGcode.Bnokta.Y = YardimciObje.LineGcode[c].Snokta.Y;
                        YardimciObje.LineGcode[c] = TempGcode;
                        continue;
                    }
             


            }

            Console.WriteLine("--------------------------");
            return SiralaList;
        }
        private int NoktaAra(List<int> kesim,List<int> Listem,double x, double y)
        {

            int xx = -1;

            for (int i = 0; i < Listem.Count; i++) //en küçük noktayı sıfıra çek
            {

                xx = Listem[i];
                sonbas = -1;
                if (sonbas == -1)
                {
                    if (Math.Abs(y - YardimciObje.LineGcode[xx].Snokta.Y) < 0.0000001 && Math.Abs(x - YardimciObje.LineGcode[xx].Snokta.X) < 0.0000001)
                    {
                        Listem.RemoveAt(i);
                        //kesim.Insert(0, xx);
                        kesim.Add(xx);
                        return xx;
                    }


                    if (Math.Abs(y - YardimciObje.LineGcode[xx].Bnokta.Y) < 0.0000001 && Math.Abs(x - YardimciObje.LineGcode[xx].Bnokta.X) < 0.0000001)
                    {
                        Listem.RemoveAt(i);
                        kesim.Add(xx);
                        return xx;
                    }

                }
                else
                {
                    if (Math.Abs(y - YardimciObje.LineGcode[xx].Snokta.Y) < 0.0000001 && Math.Abs(x - YardimciObje.LineGcode[xx].Snokta.X) < 0.0000001)
                    {
                        Listem.RemoveAt(i);
                        kesim.Insert(sonbas, xx);
                        return xx;
                    }


                    if (Math.Abs(y - YardimciObje.LineGcode[xx].Bnokta.Y) < 0.0000001 && Math.Abs(x - YardimciObje.LineGcode[xx].Bnokta.X) < 0.0000001)
                    {
                        Listem.RemoveAt(i);
                        kesim.Insert(sonbas, xx);
                        return xx;
                    }


                }


            }

            return 0;
        }
        private void button3_Click(object sender, EventArgs e)
        {

            YardimciObje.AnimateLine.Clear();
            double BrUzunluk = maxy/ 400;
            double x1=0, x2=0, y1=0, y2=0;
            Color color = new Color();
            for (int i = 3; i < listBox1.Items.Count; i++)
            {
                YardimciObje.CizgiO temp = new YardimciObje.CizgiO();


             string TempS=listBox1.Items[i].ToString();
                if (TempS == "G5") { color = Color.Green; continue; }
                if (TempS == "G6") { color = Color.Blue; continue; }
                if (TempS == "G92") { x1 = 0; x2 = 0; y1 = 0; y2 = 0; continue; }
                if (TempS.Substring(0,2) == "G0")
                {
                    x1 = x2;
                    y1 = y2;
                    string[] subs=TempS.Split (' ');
                    foreach (var sub in subs)
                    {
                      

                        if (sub.Substring(0, 1) == "X")
                        {
                            x2 = x2 + Convert.ToDouble(sub.Substring(1, sub.Length-1));
                        }
                        if (sub.Substring(0, 1) == "E")
                        {
                            y2 = y2 + Convert.ToDouble(sub.Substring(1, sub.Length-1))* BrUzunluk;
                        }
                    }
                //    Console.WriteLine("CCC= {4}  Xb={0} Yb={1} Xs={2} Ys={3}", x1, y1, x2, y2, i);
                }

                temp.Bnokta.X = x1;
                temp.Bnokta.Y = y1;
                temp.Snokta.X = x2;
                temp.Snokta.Y = y2;
                temp.Renk = color;


                YardimciObje.AnimateLine.Add(temp);

            }

           // CizimG2D.Refresh();
            CizimG2D.GrafikCiz();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            YardimciObje.AnimateLine.Clear();
            //CizimG2D.Refresh();
            CizimG2D.GrafikCiz();
        }
        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
             
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (listBox1.SelectedIndex > -1)
            { SelectAnime(listBox1.SelectedIndex + 1);
                KomutText.Text = listBox1.Items[listBox1.SelectedIndex].ToString();
                    }
        }
        public void SelectAnime(int indeks)
        {
            YardimciObje.AnimateLine.Clear();
            double BrUzunluk = maxy / 400;
            double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
            Color color = new Color();
            for (int i = 3; i < indeks; i++)
            {
                YardimciObje.CizgiO temp = new YardimciObje.CizgiO();


                string TempS = listBox1.Items[i].ToString();
                if (TempS == "G5") { color = Color.Green; continue; }
                if (TempS == "G6") { color = Color.Blue; continue; }
                if (TempS == "G92") { x1 = 0; x2 = 0; y1 = 0; y2 = 0; continue; }
                if (TempS.Substring(0, 2) == "G0")
                {
                    x1 = x2;
                    y1 = y2;
                    string[] subs = TempS.Split(' ');
                    foreach (var sub in subs)
                    {


                        if (sub.Substring(0, 1) == "X")
                        {
                            x2 = x2 + Convert.ToDouble(sub.Substring(1, sub.Length - 1));
                        }
                        if (sub.Substring(0, 1) == "E")
                        {
                            y2 = y2 + Convert.ToDouble(sub.Substring(1, sub.Length - 1)) * BrUzunluk;
                        }
                    }
               //     Console.WriteLine("CCC= {4}  Xb={0} Yb={1} Xs={2} Ys={3}", x1, y1, x2, y2, i);
                }

                temp.Bnokta.X = x1;
                temp.Bnokta.Y = y1;
                temp.Snokta.X = x2;
                temp.Snokta.Y = y2;
                temp.Renk = color;


                YardimciObje.AnimateLine.Add(temp);


            }
            label1.Text = "X1:" +Math.Round( x1,3).ToString() + " Y1:" + Math.Round(y1, 3).ToString() + " X2:" + Math.Round(x2, 3).ToString() + " Y2:" + Math.Round(y2, 3).ToString();
          //  CizimG2D.Refresh();
            CizimG2D.GrafikCiz();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Gsave();
        }

        private void Gsave()
        {
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "")
            {
                AnaPencere.ActiveForm.Text = "Boru Gcode Oluşturma  " + saveFileDialog1.FileName;
                StreamWriter sw = new StreamWriter(saveFileDialog1.FileName);
                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    sw.WriteLine(listBox1.Items[i].ToString());
                }
                sw.Close();
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Gopen();
        }

        private void Gopen()
        {
            
            openFileDialog2.ShowDialog();
            listBox1.Items.Clear();
            if (openFileDialog2.FileName != "")
            {
                AnaPencere.ActiveForm.Text ="Boru Gcode Oluşturma  " + openFileDialog2.FileName;
                StreamReader sr = new StreamReader(openFileDialog2.FileName);
                while (sr.EndOfStream==false)
                {
                    listBox1.Items.Add(sr.ReadLine());
                }
                sr.Close();

                 cap = Convert.ToDouble(listBox1.Items[1]);
                maxy = cap * Math.PI;

                textBox1.Text = listBox1.Items[2].ToString();
                KesimAdet =Convert.ToInt32( listBox1.Items[2]);

            }
        }

        private void AnaPencere_Activated(object sender, EventArgs e)
        {
            
        }

        private void AnaPencere_FormClosed(object sender, FormClosedEventArgs e)
        {
            
       
        }

        private void AnaPencere_FormClosing(object sender, FormClosingEventArgs e)
        {
         
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (B_kalinlik.Text == "") B_kalinlik.Text = "0";
            if (Uc_mesafe.Text == "") Uc_mesafe.Text = "0";
            kalinlik = Convert.ToDouble(B_kalinlik.Text);
            UcMesafe = Convert.ToDouble(Uc_mesafe.Text);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > -1)
                listBox1.Items[listBox1.SelectedIndex] = KomutText.Text;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DXFAc3D();
            

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
        YardimciObje.boruGoster = true;
            else
                YardimciObje.boruGoster = false;

                cizim3d1.Cizficiz();
            }

        private double AciNokta(YardimciObje.CizgiO temp)
        {
            double z0 = 0, z1 = 0, y0 = 0, y1 = 0;

             
            y0 = temp.Bnokta.Y;
            z0 = temp.Bnokta.Z;
            y1 = temp.Snokta.Y;
            z1 = temp.Snokta.Z;
           
 
            if (z0 == 0) z0 = 0.0000001;
            if (z1 == 0) z1 = 0.0000001;
            if (y0 == 0) y0 = 0.0000001;
            if (y1 == 0) y1 = 0.0000001;

            double m1 = -z0 / -y0;
            double m2 = -z1 / -y1;

            double tanq = (m1 - m2) / (1 + m1 * m2);
            /*
            If Option1.Value = True Then
            yaricap = DisCap / 2
            Else
            yaricap = IcCap / 2
            End If
            */
            double yaricap = 0;
            yaricap = DisCap / 2;

            double mes = Math.Sqrt(2 * (yaricap) * (yaricap));
            double mes2 = Math.Sqrt((z0 - z1) * (z0 - z1) + (y0 - y1) * (y0 - y1));
            double Aci = Math.Atan(tanq) * 200 / (Math.PI);

         if (mes >= mes2) Aci =Math.Atan(tanq) * 200 / Math.PI;

            if (mes < mes2) 
            {
                if (Aci >= 0)
               Aci = Aci - 200; 
                else
                Aci = 200 + Aci;
        }

       return  Aci;
        }
        private double NoktaAci(double Y,double Z)
        {

            double Acim = 0;

            if (Z == 0)
            {
               // Acim = 0;
               // return Acim;
                 Z = 0.00000001; }



            double Aci = Math.Atan(Y / Z);

                if (Y >= 0 && Z >= 0)
                {
                    Acim = 100 - Aci * 200 / Math.PI;
                }

               else if (Y <=0  && Z >= 0)
                {
                    Acim = 100 + Math.Abs(Aci * 200 / Math.PI);
                }


               else if (Y <= 0 && Z <= 0)
                {
                    Acim = 300 - Math.Abs(Aci * 200 / Math.PI);
                }

               else if (Y >= 0 && Z <= 0)
                {
                    Acim = 300 + Math.Abs(Aci * 200 / Math.PI);
                }
        
            return Acim;
        }
        private void button9_Click(object sender, EventArgs e)
        {

            //CizgileriSinifla();
            //GcodeYap();
           

            UcBulma();
            UclaraNotaVer();
            NoktaKesisim();
            UclaraNotaVer();
            cizim3d1.Cizficiz();
            tabControl1.SelectedIndex = 1;
           // KesimUcuSirala();
            // KesimNoktaveCizgi();
           
          // Sakla();
           SaklaCizgi();
        }

        private void NoktaKesisim()
        {
            double Ya, Za, X1, Y1, Z1,X2, Y2, Z2;
            int Sayisi = YardimciObje.Line.Count;

            for (int i = 0; i < Sayisi; i++)
            {
                if (YardimciObje.Line[i].gizli == false)
            {
           
                    for (int j = 0; j < Sayisi; j++)
                    {
                     if (i == j) continue;
                        if (YardimciObje.Line[j].gizli == false)
                        {    
       
                            X1 = YardimciObje.Line[j].Bnokta.X;
                            Y1 = YardimciObje.Line[j].Bnokta.Y;
                            Z1 = YardimciObje.Line[j].Bnokta.Z;

                            X2 = YardimciObje.Line[j].Snokta.X;
                            Y2 = YardimciObje.Line[j].Snokta.Y;
                            Z2 = YardimciObje.Line[j].Snokta.Z;

                            Ya = YardimciObje.Line[i].Bnokta.Y;
                            Za = YardimciObje.Line[i].Bnokta.Z;
                            PoliO sonuc = Denklem(Ya, Za, X1, Y1, Z1, X2, Y2, Z2);

                            int degisim = 0;
                            if (sonuc.sonuc == true)
                            {
                                YardimciObje.NoktaO tnok = new YardimciObje.NoktaO();
                                tnok.dis = YardimciObje.Line[j].dis;
                                tnok.gizli = false;


                                

                                tnok.PointXYZ.X = Math.Truncate(sonuc.x * 1000) / 1000;
                                tnok.PointXYZ.Y = Math.Truncate(sonuc.y * 1000) / 1000;
                                tnok.PointXYZ.Z = Math.Truncate(sonuc.z * 1000) / 1000;


                                /*tnok.PointXYZ.X = sonuc.x;
                                tnok.PointXYZ.Y = sonuc.y ;
                                tnok.PointXYZ.Z = sonuc.z ;*/

                                int indeks = YardimciObje.Nokta.FindIndex(c => Math.Abs( c.PointXYZ.X -tnok.PointXYZ.X)<0.01 && Math.Abs(c.PointXYZ.Y - tnok.PointXYZ.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - tnok.PointXYZ.Z )< 0.01);
                                {
                                    if (indeks == -1)

                                    {
                                        YardimciObje.Nokta.Add(tnok);

                                        YardimciObje.CizgiO Temp = new YardimciObje.CizgiO();
                                        Point3D point3D = new Point3D();
                                        Temp = YardimciObje.Line[j];
                                        point3D = Temp.Snokta;
                                        Temp.Snokta = tnok.PointXYZ;
                                        YardimciObje.Line[j] = Temp;
                                        YardimciObje.CizgiO Temp2 = new YardimciObje.CizgiO();
                                        Temp2 = GcodeOlustur.ExtensionMethods.DeepCopy(YardimciObje.Line[j]);
                                        Temp2.Bnokta = tnok.PointXYZ;
                                        Temp2.Snokta = point3D;
                                        YardimciObje.Line.Add(Temp2);
                                        degisim = YardimciObje.Line.Count-1;//Değişim numarası
                                    }
                                }
                            }

                            Ya = YardimciObje.Line[i].Snokta.Y;
                            Za = YardimciObje.Line[i].Snokta.Z;
                            PoliO sonuc2 = Denklem(Ya, Za, X1, Y1, Z1, X2, Y2, Z2);

                            if (sonuc2.sonuc == true)
                            {
                                YardimciObje.NoktaO tnok = new YardimciObje.NoktaO();
                                tnok.dis= YardimciObje.Line[j].dis;
                                tnok.gizli = false;
                                tnok.PointXYZ.X = Math.Truncate(sonuc2.x * 1000) / 1000;
                                tnok.PointXYZ.Y = Math.Truncate(sonuc2.y * 1000) / 1000;
                                tnok.PointXYZ.Z = Math.Truncate(sonuc2.z * 1000) / 1000;





                                int indeks = YardimciObje.Nokta.FindIndex(c => Math.Abs(c.PointXYZ.X - tnok.PointXYZ.X) < 0.01 && Math.Abs(c.PointXYZ.Y - tnok.PointXYZ.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - tnok.PointXYZ.Z) < 0.01);
                                if (indeks == -1)
                                {
                                    YardimciObje.Nokta.Add(tnok);
 



                                    double Uzunluk2 = Math.Sqrt(Math.Pow(YardimciObje.Line[j].Snokta.X - YardimciObje.Line[j].Bnokta.X, 2) +
                                                                Math.Pow(YardimciObje.Line[j].Snokta.Y - YardimciObje.Line[j].Bnokta.Y, 2) +
                                                                Math.Pow(YardimciObje.Line[j].Snokta.Z - YardimciObje.Line[j].Bnokta.Z, 2));
                                    if (degisim != 0)
                                    {
                                        double Uzunluk = Math.Sqrt(Math.Pow(tnok.PointXYZ.X - YardimciObje.Line[j].Bnokta.X, 2) +
                                                        Math.Pow(tnok.PointXYZ.Y - YardimciObje.Line[j].Bnokta.Y, 2) +
                                                        Math.Pow(tnok.PointXYZ.Z - YardimciObje.Line[j].Bnokta.Z, 2));

                                        if (Uzunluk2 < Uzunluk)
                                        {

                                            YardimciObje.CizgiO Temp = new YardimciObje.CizgiO();
                                            Point3D point3D = new Point3D();
                                            Temp = YardimciObje.Line[degisim];
                                            point3D = Temp.Snokta;
                                            Temp.Snokta = tnok.PointXYZ;
                                            YardimciObje.Line[degisim] = Temp;
                                            YardimciObje.CizgiO Temp2 = new YardimciObje.CizgiO();
                                            Temp2 = GcodeOlustur.ExtensionMethods.DeepCopy(YardimciObje.Line[j]);
                                            Temp2.Bnokta = tnok.PointXYZ;
                                            Temp2.Snokta = point3D;
                                            YardimciObje.Line.Add(Temp2);

                                        }
                                        else
                                        {
                                            YardimciObje.CizgiO Temp = new YardimciObje.CizgiO();
                                            Point3D point3D = new Point3D();
                                            Temp = YardimciObje.Line[j];
                                            point3D = Temp.Snokta;
                                            Temp.Snokta = tnok.PointXYZ;
                                            YardimciObje.Line[j] = Temp;
                                            YardimciObje.CizgiO Temp2 = new YardimciObje.CizgiO();
                                            Temp2 = GcodeOlustur.ExtensionMethods.DeepCopy(YardimciObje.Line[j]);
                                            Temp2.Bnokta = tnok.PointXYZ;
                                            Temp2.Snokta = point3D;
                                            YardimciObje.Line.Add(Temp2);

                                        }

                                    }
                                    else
                                    {

                                        YardimciObje.CizgiO Temp = new YardimciObje.CizgiO();
                                        Point3D point3D = new Point3D();
                                        Temp = YardimciObje.Line[j];
                                        point3D = Temp.Snokta;
                                        Temp.Snokta = tnok.PointXYZ;
                                        YardimciObje.Line[j] = Temp;
                                        YardimciObje.CizgiO Temp2 = new YardimciObje.CizgiO();
                                        Temp2 = GcodeOlustur.ExtensionMethods.DeepCopy(YardimciObje.Line[j]);
                                        Temp2.Bnokta = tnok.PointXYZ;
                                        Temp2.Snokta = point3D;
                                        YardimciObje.Line.Add(Temp2);


                                    }




                                }

                            }


                        }
                    }
                } 
        
        }



        }



            private void KesimNoktaveCizgi()
        {

            //Kesilecek kısımdaki çizgileri ve oktaları yeni bir değişkene atadık
            YardimciObje.NoktaG.Clear();
            YardimciObje.LineG.Clear();
            YardimciObje.DNo = 0;
            for (int i = 0; i < YardimciObje.Line.Count; i++)
            {
                if ( YardimciObje.Line[i].gizli == false && YardimciObje.Line[i].dis== true)

                {
                    if (YardimciObje.Line[i].Bnokta != YardimciObje.Line[i].Snokta)
                    {

                        int sonuc = YardimciObje.NoktaG.FindIndex(c => Math.Abs(c.PointXYZ.X - YardimciObje.Line[i].Bnokta.X) < 0.01 && Math.Abs(c.PointXYZ.Y - YardimciObje.Line[i].Bnokta.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - YardimciObje.Line[i].Bnokta.Z) < 0.01);
                        YardimciObje.NoktaO tNk = new YardimciObje.NoktaO();

                        if (sonuc == -1)
                        {

                            tNk.dis = YardimciObje.Line[i].dis;
                            tNk.gizli = YardimciObje.Line[i].gizli;
                            tNk.PointXYZ.X = YardimciObje.Line[i].Bnokta.X;
                            tNk.PointXYZ.Y = YardimciObje.Line[i].Bnokta.Y;
                            tNk.PointXYZ.Z = YardimciObje.Line[i].Bnokta.Z;
                            YardimciObje.DNo = YardimciObje.DNo + 1;
                            tNk.No = YardimciObje.DNo;
                            YardimciObje.NoktaG.Add(tNk);
                        }
                        else
                        {
                            tNk = YardimciObje.NoktaG[sonuc];
                        }

                        int sonuc1 = YardimciObje.NoktaG.FindIndex(c => Math.Abs(c.PointXYZ.X - YardimciObje.Line[i].Snokta.X) < 0.01 && Math.Abs(c.PointXYZ.Y - YardimciObje.Line[i].Snokta.Y) < 0.01 && Math.Abs(c.PointXYZ.Z - YardimciObje.Line[i].Snokta.Z) < 0.01);
                        YardimciObje.NoktaO tNk2 = new YardimciObje.NoktaO();
                        if (sonuc1 == -1)
                        {
                            
                            tNk2.dis = YardimciObje.Line[i].dis;
                            tNk2.gizli = YardimciObje.Line[i].gizli;
                            tNk2.PointXYZ.X = YardimciObje.Line[i].Snokta.X;
                            tNk2.PointXYZ.Y = YardimciObje.Line[i].Snokta.Y;
                            tNk2.PointXYZ.Z = YardimciObje.Line[i].Snokta.Z;
                            YardimciObje.DNo = YardimciObje.DNo + 1;
                            tNk2.No = YardimciObje.DNo;
                            YardimciObje.NoktaG.Add(tNk2);
                           
                        }
                        else
                        {
                            tNk2 = YardimciObje.NoktaG[sonuc1];
                        }

                        //Çizgileri Ekle
                        YardimciObje.CizgiO Tline = new YardimciObje.CizgiO();
                        Tline.dis = YardimciObje.Line[i].dis;
                        Tline.BN = tNk.No;
                        Tline.SN = tNk2.No;
                        Tline.Kullan = false;
                        YardimciObje.LineG.Add(Tline);
                    }

                }


                }


            //Aynı çizgileri sil
            for (int i = 0; i < YardimciObje.LineG.Count; i++)
            {
                for (int j = i + 1; j < YardimciObje.LineG.Count; j++)
                {
                    if ((YardimciObje.LineG[i].SN == YardimciObje.LineG[j].SN && YardimciObje.LineG[i].BN == YardimciObje.LineG[j].BN ) ||
                        (YardimciObje.LineG[i].SN == YardimciObje.LineG[j].BN && YardimciObje.LineG[i].BN == YardimciObje.LineG[j].SN))
                    {
                        YardimciObje.LineG.RemoveAt(j);
                        continue;
                    }

                }


                }

          
                // Çizgileri Sırala

                for (int i = 0; i < YardimciObje.LineG.Count; i++)
            {
                for (int j = i+1; j < YardimciObje.LineG.Count; j++)
                {
                    if (YardimciObje.LineG[i].BN == YardimciObje.LineG[j].BN)
                    {
                        

                        var item = YardimciObje.LineG[j];
                        int Temp = item.BN;
                        item.BN = item.SN;
                        item.SN = Temp;

                        YardimciObje.LineG.RemoveAt(j);
                        YardimciObje.LineG.Insert(i+1, item);
                        continue;
                    }


                    if (YardimciObje.LineG[i].BN == YardimciObje.LineG[j].SN)
                    {
                     

                        var item = YardimciObje.LineG[j];
                       

                        YardimciObje.LineG.RemoveAt(j);
                        YardimciObje.LineG.Insert(i + 1, item);
                        continue;
                    }

                }

            }

  
 
    }
        private void SaklaCizgi()
        {
            try
            {
                FileStream stream = File.Create("cizgi.DXF");
                StreamWriter sw = new StreamWriter(stream);
                #region Sabit
                sw.WriteLine("0");
                sw.WriteLine("SECTION");
                sw.WriteLine("2");
                sw.WriteLine("HEADER");
                sw.WriteLine("9");
                sw.WriteLine("$EXTMIN");
                sw.WriteLine("10");
                sw.WriteLine("0");
                sw.WriteLine("20");
                sw.WriteLine("0");
                sw.WriteLine("30");
                sw.WriteLine("0");
                sw.WriteLine("9");
                sw.WriteLine("$EXTMAX");
                sw.WriteLine("10");
                sw.WriteLine(100);
                sw.WriteLine("20");
                sw.WriteLine(200);
                sw.WriteLine("30");
                sw.WriteLine("0");
                sw.WriteLine("9");

                sw.WriteLine("$LIMMIN");
                sw.WriteLine("10");
                sw.WriteLine("0.0");
                sw.WriteLine("20");
                sw.WriteLine("0.0");
                sw.WriteLine("9");
                sw.WriteLine("$LIMMAX");
                sw.WriteLine("10");
                sw.WriteLine("10.0");
                sw.WriteLine("20");
                sw.WriteLine("7.0");
                sw.WriteLine("9");
                sw.WriteLine("$VIEWSIZE");
                sw.WriteLine("40");
                sw.WriteLine("7.0");
                sw.WriteLine("9");
                sw.WriteLine("$CELTYPE");
                sw.WriteLine("6");
                sw.WriteLine("BYLAYER");
                sw.WriteLine("9");
                sw.WriteLine("$CECOLOR");
                sw.WriteLine("62");
                sw.WriteLine("256");
                sw.WriteLine("0");
                sw.WriteLine("ENDSEC");
                sw.WriteLine("0");
                sw.WriteLine("SECTION");
                sw.WriteLine("2");
                sw.WriteLine("TABLES");
                sw.WriteLine("0");
                sw.WriteLine("TABLE");
                sw.WriteLine("2");
                sw.WriteLine("LTYPE");
                sw.WriteLine("70");
                sw.WriteLine("1");
                sw.WriteLine("0");
                sw.WriteLine("LTYPE");
                sw.WriteLine("2");
                sw.WriteLine("CONTINUOUS");
                sw.WriteLine("70");
                sw.WriteLine("64");
                sw.WriteLine("3");
                sw.WriteLine("Solid Line");
                sw.WriteLine("72");
                sw.WriteLine("65");
                sw.WriteLine("73");
                sw.WriteLine("0");
                sw.WriteLine("40");
                sw.WriteLine("0.0");
                sw.WriteLine("0");
                sw.WriteLine("ENDTAB");
                sw.WriteLine("0");
                sw.WriteLine("TABLE");
                sw.WriteLine("2");
                sw.WriteLine("LAYER");
                sw.WriteLine("70");
                sw.WriteLine(2);
                sw.WriteLine("0");

                //Layer Ekleme              

                sw.WriteLine("LAYER");
                sw.WriteLine("2");
                sw.WriteLine("Yeni");
                sw.WriteLine("70");
                sw.WriteLine("0");
                sw.WriteLine("62");
                sw.WriteLine(3);
                sw.WriteLine("6");
                sw.WriteLine("CONTINUOUS");
                sw.WriteLine("0");

                sw.WriteLine("LAYER");
                sw.WriteLine("3");
                sw.WriteLine("Eski");
                sw.WriteLine("70");
                sw.WriteLine("0");
                sw.WriteLine("62");
                sw.WriteLine(3);
                sw.WriteLine("6");
                sw.WriteLine("CONTINUOUS");
                sw.WriteLine("0");

                //Layer Ekleme
                sw.WriteLine("ENDTAB");
                sw.WriteLine("0");
                sw.WriteLine("ENDSEC");
                sw.WriteLine("0");
                sw.WriteLine("SECTION");
                sw.WriteLine("2");
                sw.WriteLine("ENTITIES");
                sw.WriteLine("0");

                #endregion


                for (int x = 0; x < YardimciObje.Nokta.Count; x++)
                {
                    double AciNok = NoktaAci(YardimciObje.Nokta[x].PointXYZ.Y, YardimciObje.Nokta[x].PointXYZ.Z);
 
                        sw.WriteLine("CIRCLE");
                        sw.WriteLine("5");
                        sw.WriteLine("3A");
                        sw.WriteLine("8");


                    if (YardimciObje.Nokta[x].dis == true)
                        sw.WriteLine("Yeni");
                    else
                        sw.WriteLine("Eski");

                        sw.WriteLine("10");
                        sw.WriteLine(YardimciObje.Nokta[x].PointXYZ.X);
                        sw.WriteLine("20");
                        sw.WriteLine(AciNok);
                        sw.WriteLine("30");
                        sw.WriteLine(0);
                        sw.WriteLine("40");
                        sw.WriteLine(0.1);
                        sw.WriteLine(0);



                    sw.WriteLine("TEXT");
                    sw.WriteLine("2");
                    if (YardimciObje.Nokta[x].dis == true)
                        sw.WriteLine("Yeni");
                    else
                        sw.WriteLine("Eski");
                    sw.WriteLine("10");
                    sw.WriteLine(YardimciObje.Nokta[x].PointXYZ.X);
                    sw.WriteLine("20");
                    sw.WriteLine(AciNok);
                    sw.WriteLine("40");
                    sw.WriteLine("2");
                    sw.WriteLine("1");
                    sw.WriteLine(YardimciObje.Nokta[x].No);
                    sw.WriteLine("41");
                    sw.WriteLine("0.75");
                    sw.WriteLine("50");
                    sw.WriteLine("0");
                    sw.WriteLine("0");


                }
 
                for (int c = 0; c < YardimciObje.Line.Count; c++)
                {

                    if (YardimciObje.Line[c].gizli == true) continue;
                    //  if (YardimciObje.Line[c].BN == 0 || YardimciObje.Line[c].SN == 0) continue;



/*

                    if ((DNoToLNo(YardimciObje.Line[c].BN) == 19 || DNoToLNo(YardimciObje.Line[c].SN) == 13) || (DNoToLNo(YardimciObje.Line[c].BN) == 13  || DNoToLNo(YardimciObje.Line[c].SN) == 19) )
                    {
                    double f =
                        YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].BN)].PointXYZ.Y;
                     double v=   YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].BN)].PointXYZ.Z;


                }
*/



                        double AciBas = NoktaAci(YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].BN)].PointXYZ.Y, YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].BN)].PointXYZ.Z);
                        double AciSon = NoktaAci(YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].SN)].PointXYZ.Y, YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].SN)].PointXYZ.Z);
                    //if (AciBas == AciSon) continue;

                        double X1 = YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].BN)].PointXYZ.X;
                        double Y1 = AciBas;
                        double Z1 = 0;

                        double X2 = YardimciObje.Nokta[DNoToLNo(YardimciObje.Line[c].SN)].PointXYZ.X;
                        double Y2 = AciSon;
                        double Z2 = 0;

                        sw.WriteLine("LINE");
                        sw.WriteLine("5");
                        sw.WriteLine("F27");
                        sw.WriteLine("8");

                    if(YardimciObje.Line[c].dis==true)
                        sw.WriteLine("Yeni");
                    else
                        sw.WriteLine("Eski");

                        sw.WriteLine("10");
                        sw.WriteLine(X1);
                        sw.WriteLine("20");
                        sw.WriteLine(Y1);
                        sw.WriteLine("30");
                        sw.WriteLine(Z1);

                        sw.WriteLine("11");
                        sw.WriteLine(X2);
                        sw.WriteLine("21");
                        sw.WriteLine(Y2);
                        sw.WriteLine("31");
                        sw.WriteLine(Z2);
                        sw.WriteLine("0");



                    sw.WriteLine("TEXT");
                    sw.WriteLine("2");
                    sw.WriteLine("Eski");
                    sw.WriteLine("10");
                    sw.WriteLine(X1);
                    sw.WriteLine("20");
                    sw.WriteLine(Y1);
                    sw.WriteLine("40");
                    sw.WriteLine("2");
                    sw.WriteLine("1");
                    sw.WriteLine(YardimciObje.Line[c].BN);
                    sw.WriteLine("41");
                    sw.WriteLine("0.75");
                    sw.WriteLine("50");
                    sw.WriteLine("0");
                    sw.WriteLine("0");


                   /* sw.WriteLine("TEXT");
                    sw.WriteLine("8");
                    sw.WriteLine("Eski");
                    sw.WriteLine("10");
                    sw.WriteLine(X2);
                    sw.WriteLine("20");
                    sw.WriteLine(Y2);
                    sw.WriteLine("40");
                    sw.WriteLine("10");
                    sw.WriteLine("1");
                    sw.WriteLine(YardimciObje.Line[c].SN);
                    sw.WriteLine("41");
                    sw.WriteLine("0.75");
                    sw.WriteLine("50");
                    sw.WriteLine("0");
                    sw.WriteLine("0");*/



                }

                sw.WriteLine("ENDSEC");
                sw.WriteLine("  0");
                sw.WriteLine("EOF");
                sw.Close();
            }
            catch {
                Console.WriteLine( "Hata");
            
            }

        }


        public static int DNoToLNo(int DugumNo) // Dugum numarasını liste numarasına ceviriyoruz
        {
            //return DugumKey[DugumNo];



            int No = YardimciObje.Nokta.FindIndex(c => c.No == DugumNo);
            return No;


        }






        private PoliO Denklem(double Ya, double Za, double X1, double Y1, double Z1, double X2, double Y2, double Z2)
        {


            double z = 0, y = 0, x = 0;
            bool Denklem = false;
            PoliO Sonuc = new PoliO();


            if ((Y1 - Y2) == 0 || (Z1 - Z2) == 0) { Sonuc.sonuc = false; return Sonuc; }
            if ((Za * (Y1 - Y2)) == 0) { Sonuc.sonuc = false; return Sonuc; }
            if ((Za * (Y1 - Y2)) - 1 / (Z1 - Z2) == 0) { Sonuc.sonuc = false; return Sonuc; }




            z = (Y1 / (Y1 - Y2) - Z1 / (Z1 - Z2)) / (Ya / (Za * (Y1 - Y2)) - 1 / (Z1 - Z2));
            y = z * Ya / Za;


           // double Xa = X1 - (Y1 - y) * (X1 - X2) / (Y1 - Y2);
            x = X1 - (Z1 - z) * (X1 - X2) / (Z1 - Z2);

            if
            ((Y2 <= y && y <= Y1) && (Z2 <= z && z <= Z1) ||
            (Y2 <= y && y <= Y1) && (Z2 >= z && z >= Z1) ||
            (Y2 >= y && y >= Y1) && (Z2 <= z && z <= Z1) ||
            (Y2 >= y && y >= Y1) && (Z2 >= z && z >= Z1)
            )
            {
                Sonuc.x = x;
                Sonuc.y = y;
                Sonuc.z = z;

                Sonuc.sonuc = true;
                return Sonuc;
            }
            else
            {
                Sonuc.sonuc = false;
            }

            Sonuc.sonuc = false;
            return Sonuc;
        }
        private void KesimUcuSirala()
        {
            YardimciObje.SiralanmisCizgi.Clear();
            // En küçük X bul vo ordan başla
            double XSnoktaD = 1000;//Sıfırdan büyük bir sayı
            double XSnoktaI = 1000;//Sıfırdan büyük bir sayı

            double XnoktaD = 1000;//Sıfırdan büyük bir sayı
            double XnoktaI = 1000;//Sıfırdan büyük bir sayı

            int DBasNokta = 0;
            int IBasNokta = 0;
            for (int i = 0; i < YardimciObje.Line.Count; i++)
            {
                //Dis Baslangic Nokta Bul
                if (YardimciObje.Line[i].dis == false && YardimciObje.Line[i].gizli==false)
                {
                    if (YardimciObje.Line[i].Bnokta.X < YardimciObje.Line[i].Snokta.X)
                        XnoktaD = YardimciObje.Line[i].Bnokta.X;
                    else
                        XnoktaD = YardimciObje.Line[i].Snokta.X;


                    if (XSnoktaD < XnoktaD)
                    {
                        XSnoktaD = XnoktaD;
                        DBasNokta = i;
                    }
                }

                //İç Baslangic Nokta Bul
                if (YardimciObje.Line[i].dis == true && YardimciObje.Line[i].gizli == false)
                {
                    if (YardimciObje.Line[i].Bnokta.X < YardimciObje.Line[i].Snokta.X)
                        XnoktaI = YardimciObje.Line[i].Bnokta.X;
                    else
                        XnoktaI = YardimciObje.Line[i].Snokta.X;
                    if (XSnoktaI < XnoktaI)
                    {
                        XSnoktaI = XnoktaI;
                        IBasNokta = i;
                    }
                }
 

            }




        }
        public void CizgileriSinifla()
        {

        /*  var LineCopy=  ExtensionMethods.DeepCopy(YardimciObje.Line);

       
            for (int c = 0; c < LineCopy.Count; c++)
            {
                 
                        double AciBas = MerkezAci(LineCopy[c].Bnokta.Y, LineCopy[c].Bnokta.Z);
                        double AciSon = MerkezAci(LineCopy[c].Snokta.Y, LineCopy[c].Snokta.Z);
                        if (LineCopy[c].icdis == true && LineCopy[c].gizli == false)
                        {
                    YardimciObje.CizgiO Temp = new YardimciObje.CizgiO();

                    Temp.Bnokta.X = LineCopy[c].Bnokta.X;
                    Temp.Bnokta.Y =AciBas * 2.0 * Math.PI * DisCap / 2 / 400;
                    Temp.Bnokta.Z = 0;

                    Temp.Snokta.X = LineCopy[c].Snokta.X;
                    Temp.Snokta.Y = AciSon * 2.0 * Math.PI * DisCap / 2 / 400;
                    Temp.Snokta.Z = 0;

                    Temp.layer = "kes";
                    Temp.icdis = true;
                    Temp.gizli = false;
                    YardimciObje.Line.Add(Temp);

                        }
                    }
        */
        }
        private double MerkezAci(double Y, double Z)
        {
            double z0 = 0, z1 = 0, y0 = 0, y1 = 0;


            y0 = DisCap / 2;
            z0 = 0;// DisCap / 2;
            y1 = Y;
            z1 = Z;


            if (z0 == 0) z0 = 0.0000001;
            if (z1 == 0) z1 = 0.0000001;
            if (y0 == 0) y0 = 0.0000001;
            if (y1 == 0) y1 = 0.0000001;

            double m1 = z0 / y0;
            double m2 = z1 / y1;

            double tanq = (m2 - m1) / (1 + m1 * m2);
            double Aci = Math.Atan(tanq) * 200 / (Math.PI);
            double Acim = 0;
            if (Math.Sign(y1) >= 0 && Math.Sign(z1) >= 0)
            {//+ +
                Aci = Aci;

            }
            else if (Math.Sign(y1) <= 0 && Math.Sign(z1) >= 0)
            {//- +
                Aci = 200 + Aci;
            }

            else if (Math.Sign(y1) <= 0 && Math.Sign(z1) <= 0)
            {//- -
                Aci = Aci + 200;
            }
           

            else if (Math.Sign(y1) >= 0 && Math.Sign(z1) <= 0)
            {//+ -
                Aci = 400 + Aci;
            }

            ////////////////////////////////////////////////////////////
            if (Y >= 0 && Z >= 0)
            {
                Acim = 100 - Aci * 200 / Math.PI;
            }

            else if (Y < 0 && Z > 0)
            {
                Acim = 100 + Math.Abs(Aci * 200 / Math.PI);
            }


            else if (Y < 0 && Z < 0)
            {
                Acim = 300 - Math.Abs(Aci * 200 / Math.PI);
            }

            else if (Y > 0 && Z < 0)
            {
                Acim = 300 + Math.Abs(Aci * 200 / Math.PI);
            }

            return Acim;

 



            double yaricap = 0;
        yaricap = DisCap / 2;

        double mes = Math.Sqrt(2 * (yaricap) * (yaricap));
        double mes2 = Math.Sqrt((z0 - z1) * (z0 - z1) + (y0 - y1) * (y0 - y1));
        double Aci2 = Math.Atan(tanq) * 200 / (Math.PI);

        if (mes >= mes2) Aci2 = Math.Atan(tanq) * 200 / Math.PI;

        if (mes < mes2)
        {
            if (Aci2 >= 0)
                Aci2 = Aci2 - 200;
            else
                Aci2 = 200 + Aci2;
        }



            if (Z == 0)
            {
                // Acim = 0;
                // return Acim;
                Z = 0.00000001;
            }




            return Aci;
        }
        private void Sakla()
        {
            try
            {
                FileStream stream = File.Create("deneme.DXF");
                StreamWriter sw = new StreamWriter(stream);

                sw.WriteLine("0");
                sw.WriteLine("SECTION");
                sw.WriteLine("2");
                sw.WriteLine("HEADER");
                sw.WriteLine("9");
                sw.WriteLine("$EXTMIN");
                sw.WriteLine("10");
                sw.WriteLine("0");
                sw.WriteLine("20");
                sw.WriteLine("0");
                sw.WriteLine("30");
                sw.WriteLine("0");
                sw.WriteLine("9");
                sw.WriteLine("$EXTMAX");
                sw.WriteLine("10");
                sw.WriteLine(100);
                sw.WriteLine("20");
                sw.WriteLine(200);
                sw.WriteLine("30");
                sw.WriteLine("0");
                sw.WriteLine("9");

                sw.WriteLine("$LIMMIN");
                sw.WriteLine("10");
                sw.WriteLine("0.0");
                sw.WriteLine("20");
                sw.WriteLine("0.0");
                sw.WriteLine("9");
                sw.WriteLine("$LIMMAX");
                sw.WriteLine("10");
                sw.WriteLine("10.0");
                sw.WriteLine("20");
                sw.WriteLine("7.0");
                sw.WriteLine("9");
                sw.WriteLine("$VIEWSIZE");
                sw.WriteLine("40");
                sw.WriteLine("7.0");
                sw.WriteLine("9");
                sw.WriteLine("$CELTYPE");
                sw.WriteLine("6");
                sw.WriteLine("BYLAYER");
                sw.WriteLine("9");
                sw.WriteLine("$CECOLOR");
                sw.WriteLine("62");
                sw.WriteLine("256");
                sw.WriteLine("0");
                sw.WriteLine("ENDSEC");
                sw.WriteLine("0");
                sw.WriteLine("SECTION");
                sw.WriteLine("2");
                sw.WriteLine("TABLES");
                sw.WriteLine("0");
                sw.WriteLine("TABLE");
                sw.WriteLine("2");
                sw.WriteLine("LTYPE");
                sw.WriteLine("70");
                sw.WriteLine("1");
                sw.WriteLine("0");
                sw.WriteLine("LTYPE");
                sw.WriteLine("2");
                sw.WriteLine("CONTINUOUS");
                sw.WriteLine("70");
                sw.WriteLine("64");
                sw.WriteLine("3");
                sw.WriteLine("Solid Line");
                sw.WriteLine("72");
                sw.WriteLine("65");
                sw.WriteLine("73");
                sw.WriteLine("0");
                sw.WriteLine("40");
                sw.WriteLine("0.0");
                sw.WriteLine("0");
                sw.WriteLine("ENDTAB");
                sw.WriteLine("0");
                sw.WriteLine("TABLE");
                sw.WriteLine("2");
                sw.WriteLine("LAYER");
                sw.WriteLine("70");
                sw.WriteLine(2);
                sw.WriteLine("0");

                //Layer Ekleme              

                sw.WriteLine("LAYER");
                sw.WriteLine("2");
                sw.WriteLine("Yeni");
                sw.WriteLine("70");
                sw.WriteLine("0");
                sw.WriteLine("62");
                sw.WriteLine(3);
                sw.WriteLine("6");
                sw.WriteLine("CONTINUOUS");
                sw.WriteLine("0");

                sw.WriteLine("LAYER");
                sw.WriteLine("3");
                sw.WriteLine("Eski");
                sw.WriteLine("70");
                sw.WriteLine("0");
                sw.WriteLine("62");
                sw.WriteLine(3);
                sw.WriteLine("6");
                sw.WriteLine("CONTINUOUS");
                sw.WriteLine("0");

                //Layer Ekleme
                sw.WriteLine("ENDTAB");
                sw.WriteLine("0");
                sw.WriteLine("ENDSEC");
                sw.WriteLine("0");
                sw.WriteLine("SECTION");
                sw.WriteLine("2");
                sw.WriteLine("ENTITIES");
                sw.WriteLine("0");

   
                for (int x = 0; x < YardimciObje.Nokta.Count; x++)
                {

                   // double AciBas2 = MerkezAci(YardimciObje.Nokta[x].PointXYZ.Y, YardimciObje.Nokta[x].PointXYZ.Z);
                    double AciBas = NoktaAci(YardimciObje.Nokta[x].PointXYZ.Y, YardimciObje.Nokta[x].PointXYZ.Z);


                     
                        sw.WriteLine("CIRCLE");
                        sw.WriteLine("5");
                        sw.WriteLine("3A");
                        sw.WriteLine("8");

                    if (YardimciObje.Nokta[x].dis == true)
                        sw.WriteLine("Yeni");
                    else
                        sw.WriteLine("Eski");

                        sw.WriteLine("10");
                        sw.WriteLine(YardimciObje.Nokta[x].PointXYZ.X);
                        sw.WriteLine("20");
                        sw.WriteLine(AciBas);
                        sw.WriteLine("30");
                        sw.WriteLine(0);
                        sw.WriteLine("40");
                        sw.WriteLine(0.1);
                        sw.WriteLine(0);
                  
                    /*
                    sw.WriteLine("TEXT");
                    sw.WriteLine("8");

                    if (YardimciObje.Nokta[x].dis == true)
                        sw.WriteLine("Yeni");
                    else
                        sw.WriteLine("Eski");

                    sw.WriteLine("10");
                    sw.WriteLine(YardimciObje.Nokta[x].PointXYZ.X);
                    sw.WriteLine("20");
                    sw.WriteLine(AciBas);
                    sw.WriteLine("40");
                    sw.WriteLine("10");
                    sw.WriteLine("1");
                    sw.WriteLine(x);
                    sw.WriteLine("41");
                    sw.WriteLine("0.75");
                    sw.WriteLine("50");
                    sw.WriteLine("0");
                    sw.WriteLine("0");
                    */





                }
 
                for (int c = 0; c < YardimciObje.Line.Count; c++)
                {

                /*
                    if (YardimciObje.Line[c].dis == true && YardimciObje.Line[c].gizli == false)
                    {
                        double AciBas = NoktaAci(YardimciObje.Line[c].Bnokta.Y, YardimciObje.Line[c].Bnokta.Z);
                        double AciSon = NoktaAci(YardimciObje.Line[c].Snokta.Y, YardimciObje.Line[c].Snokta.Z);
                        if (AciBas == AciSon) continue;

                        double X1 = YardimciObje.Line[c].Bnokta.X;
                        double Y1 = AciBas;
                        double Z1 = 0;

                        double X2 = YardimciObje.Line[c].Snokta.X;
                        double Y2 = AciSon;
                        double Z2 = 0;
 
                        sw.WriteLine("LINE");
                        sw.WriteLine("5");
                        sw.WriteLine("F27");
                        sw.WriteLine("8");
                        sw.WriteLine("Yeni");
                        sw.WriteLine("10");
                        sw.WriteLine(X1);
                        sw.WriteLine("20");
                        sw.WriteLine(Y1);
                        sw.WriteLine("30");
                        sw.WriteLine(Z1);
                        sw.WriteLine("11");
                        sw.WriteLine(X2);
                        sw.WriteLine("21");
                        sw.WriteLine(Y2);
                        sw.WriteLine("31");
                        sw.WriteLine(Z2);
                        sw.WriteLine("0");
 
                    }
 

                    if (YardimciObje.Line[c].dis == false && YardimciObje.Line[c].gizli == false)
                    {
                        double AciBas = NoktaAci(YardimciObje.Line[c].Bnokta.Y, YardimciObje.Line[c].Bnokta.Z);
                        double AciSon = NoktaAci(YardimciObje.Line[c].Snokta.Y, YardimciObje.Line[c].Snokta.Z);
                        if (AciBas == AciSon) continue;

                        double X1 = YardimciObje.Line[c].Bnokta.X;
                        double Y1 = AciBas;
                        double Z1 = 0;

                        double X2 = YardimciObje.Line[c].Snokta.X;
                        double Y2 = AciSon;
                        double Z2 = 0;


                        sw.WriteLine("LINE");
                        sw.WriteLine("5");
                        sw.WriteLine("F27");
                        sw.WriteLine("8");
                        sw.WriteLine("Eski");
                        sw.WriteLine("10");
                        sw.WriteLine(X1);
                        sw.WriteLine("20");
                        sw.WriteLine(Y1);
                        sw.WriteLine("30");
                        sw.WriteLine(Z1);
                        sw.WriteLine("11");
                        sw.WriteLine(X2);
                        sw.WriteLine("21");
                        sw.WriteLine(Y2);
                        sw.WriteLine("31");
                        sw.WriteLine(Z2);
                        sw.WriteLine("0");



                    }*/
 
                }
 
                sw.WriteLine("ENDSEC");
                sw.WriteLine("  0");
                sw.WriteLine("EOF");
                sw.Close();
            }
            catch { }

        }
        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
    class GFG : IComparer<YardimciObje.siralamao>
    {
        public int Compare(YardimciObje.siralamao x, YardimciObje.siralamao y)
        {
            

            // CompareTo() method
            return x.xboy.CompareTo(y.xboy);

        }
    }
}
